prompt #---------------------#
prompt #- Pomocne procedury -#
prompt #---------------------#

create or replace procedure SMAZ_VSECHNY_TABULKY AS
-- pokud v logu bude uvedeno, ze nektery objekt nebyl zrusen, protoze na nej jiny jeste existujici objekt stavi, spust proceduru opakovane, dokud se nezrusi vse
begin
  for iRec in 
    (select distinct OBJECT_TYPE, OBJECT_NAME,
      'drop '||OBJECT_TYPE||' "'||OBJECT_NAME||'"'||
      case OBJECT_TYPE when 'TABLE' then ' cascade constraints purge' else ' ' end as PRIKAZ
    from USER_OBJECTS where OBJECT_NAME not in ('SMAZ_VSECHNY_TABULKY', 'VYPNI_CIZI_KLICE', 'ZAPNI_CIZI_KLICE', 'VYMAZ_DATA_VSECH_TABULEK')
    ) loop
        begin
          dbms_output.put_line('Prikaz: '||irec.prikaz);
        execute immediate iRec.prikaz;
        exception
          when others then dbms_output.put_line('NEPOVEDLO SE!');
        end;
      end loop;
end;
/

create or replace procedure VYPNI_CIZI_KLICE as 
begin
  for cur in (select CONSTRAINT_NAME, TABLE_NAME from USER_CONSTRAINTS where CONSTRAINT_TYPE = 'R' ) 
  loop
    execute immediate 'alter table '||cur.TABLE_NAME||' modify constraint "'||cur.CONSTRAINT_NAME||'" DISABLE';
  end loop;
end VYPNI_CIZI_KLICE;
/


create or replace procedure ZAPNI_CIZI_KLICE as 
begin
  for cur in (select CONSTRAINT_NAME, TABLE_NAME from USER_CONSTRAINTS where CONSTRAINT_TYPE = 'R' ) 
  loop
    execute immediate 'alter table '||cur.TABLE_NAME||' modify constraint "'||cur.CONSTRAINT_NAME||'" enable validate';
  end loop;
end ZAPNI_CIZI_KLICE;
/

create or replace procedure VYMAZ_DATA_VSECH_TABULEK is
begin
  -- Vymazat data vsech tabulek
  VYPNI_CIZI_KLICE;
  for v_rec in (select distinct TABLE_NAME from USER_TABLES)
  loop
    execute immediate 'truncate table '||v_rec.TABLE_NAME||' drop storage';
  end loop;
  ZAPNI_CIZI_KLICE;
  
  -- Nastavit vsechny sekvence od 1
  for v_rec in (select distinct SEQUENCE_NAME  from USER_SEQUENCES)
  loop
    execute immediate 'alter sequence '||v_rec.SEQUENCE_NAME||' restart start with 1';
  end loop;
end VYMAZ_DATA_VSECH_TABULEK;
/

prompt #------------------------#
prompt #- Zrusit stare tabulky -#
prompt #------------------------#

exec SMAZ_VSECHNY_TABULKY;

--------------------------------------------------------
--  File created - Sobota-kvÄ›tna-05-2018   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table ADRESA
--------------------------------------------------------

  CREATE TABLE "ADRESA" 
   (	"ADRESA_KEY" NUMBER(*,0), 
	"POBOCKA_KEY" NUMBER(*,0), 
	"MESTO" VARCHAR2(50 CHAR), 
	"POPISNE_CISLO" VARCHAR2(20 CHAR), 
	"ULICE" VARCHAR2(50 CHAR), 
	"PSC" VARCHAR2(10 BYTE)
   ) ;
--------------------------------------------------------
--  DDL for Table CAJ
--------------------------------------------------------

  CREATE TABLE "CAJ" 
   (	"CAJ_KEY" NUMBER(*,0), 
	"NAZEV" VARCHAR2(50 CHAR), 
	"DRUH" VARCHAR2(50 CHAR), 
	"PUVOD" VARCHAR2(50 CHAR)
   ) ;
--------------------------------------------------------
--  DDL for Table JEDNOTLIVE_KUSY
--------------------------------------------------------

  CREATE TABLE "JEDNOTLIVE_KUSY" 
   (	"JEDNOTLIVE_KUSY_KEY" NUMBER(*,0), 
	"OBCERSTVENI_KEY" NUMBER(*,0), 
	"ZBOZI_KEY" NUMBER(*,0), 
	"OBJEDNAVKA_KEY" NUMBER(*,0), 
	"POBOCKA_KEY" NUMBER(*,0), 
	"GRAMAZ" NUMBER(*,0)
   ) ;
--------------------------------------------------------
--  DDL for Table JEDNOTLIVE_PORCE
--------------------------------------------------------

  CREATE TABLE "JEDNOTLIVE_PORCE" 
   (	"JEDNOTLIVE_PORCE_KEY" NUMBER(*,0), 
	"CAJ_KEY" NUMBER(*,0), 
	"ZBOZI_KEY" NUMBER(*,0), 
	"OBJEDNAVKA_KEY" NUMBER(*,0), 
	"POBOCKA_KEY" NUMBER(*,0), 
	"GRAMAZ" NUMBER(*,0)
   ) ;
--------------------------------------------------------
--  DDL for Table NABYTEK
--------------------------------------------------------

  CREATE TABLE "NABYTEK" 
   (	"VYBAVENI_KEY" NUMBER(*,0), 
	"POBOCKA_KEY" NUMBER(*,0), 
	"OPRAVY" DATE
   ) ;
--------------------------------------------------------
--  DDL for Table NADOBI
--------------------------------------------------------

  CREATE TABLE "NADOBI" 
   (	"VYBAVENI_KEY" NUMBER(*,0), 
	"POBOCKA_KEY" NUMBER(*,0), 
	"MATERIAL" VARCHAR2(50 CHAR)
   ) ;
--------------------------------------------------------
--  DDL for Table OBCERSTVENI
--------------------------------------------------------

  CREATE TABLE "OBCERSTVENI" 
   (	"OBCERSTVENI_KEY" NUMBER(*,0), 
	"NAZEV" VARCHAR2(50 CHAR)
   ) ;
--------------------------------------------------------
--  DDL for Table OBJEDNAVKA
--------------------------------------------------------

  CREATE TABLE "OBJEDNAVKA" 
   (	"OBJEDNAVKA_KEY" NUMBER(*,0)
   ) ;
--------------------------------------------------------
--  DDL for Table OBJEDNAVKY
--------------------------------------------------------

  CREATE TABLE "OBJEDNAVKY" 
   (	"POBOCKA_KEY" NUMBER(*,0), 
	"CISLO" NUMBER(*,0), 
	"OBJEDNAVKA_KEY" NUMBER(*,0)
   ) ;
--------------------------------------------------------
--  DDL for Table POBOCKA
--------------------------------------------------------

  CREATE TABLE "POBOCKA" 
   (	"POBOCKA_KEY" NUMBER(*,0), 
	"EMAIL" VARCHAR2(50 CHAR), 
	"NAZEV" VARCHAR2(50 CHAR), 
	"TELEFONNI_CISLO" VARCHAR2(20 CHAR), 
	"WEBOVA_ADRESA" VARCHAR2(50 CHAR)
   ) ;
--------------------------------------------------------
--  DDL for Table PREDMETY_CAJ
--------------------------------------------------------

  CREATE TABLE "PREDMETY_CAJ" 
   (	"PREDMETY_KEY" NUMBER(*,0), 
	"ZBOZI_KEY" NUMBER(*,0), 
	"NAZEV" VARCHAR2(50 CHAR)
   ) ;
--------------------------------------------------------
--  DDL for Table PREDMETY_DYMKA
--------------------------------------------------------

  CREATE TABLE "PREDMETY_DYMKA" 
   (	"PREDMETY_DYMKA_KEY" NUMBER(*,0), 
	"ZBOZI_KEY" NUMBER(*,0), 
	"NAZEV" VARCHAR2(25 CHAR)
   ) ;
--------------------------------------------------------
--  DDL for Table REZERVACE
--------------------------------------------------------

  CREATE TABLE "REZERVACE" 
   (	"REZERVACE_KEY" NUMBER(*,0), 
	"POBOCKA_KEY" NUMBER(*,0), 
	"CISLO" NUMBER(*,0), 
	"CAS" DATE, 
	"DATUM" DATE, 
	"PRIJMENI" VARCHAR2(50 CHAR), 
	"JMENO" VARCHAR2(50 CHAR)
   ) ;
--------------------------------------------------------
--  DDL for Table STUL
--------------------------------------------------------

  CREATE TABLE "STUL" 
   (	"CISLO" NUMBER(*,0), 
	"POBOCKA_KEY" NUMBER(*,0)
   ) ;
--------------------------------------------------------
--  DDL for Table VODNI_DYMKA
--------------------------------------------------------

  CREATE TABLE "VODNI_DYMKA" 
   (	"VODNI_DYMKA_KEY" NUMBER(*,0), 
	"ZBOZI_KEY" NUMBER(*,0), 
	"OBJEDNAVKA_KEY" NUMBER(*,0), 
	"POBOCKA_KEY" NUMBER(*,0), 
	"TYP" VARCHAR2(50 BYTE), 
	"VYSKA" NUMBER(*,0)
   ) ;
--------------------------------------------------------
--  DDL for Table VYBAVENI
--------------------------------------------------------

  CREATE TABLE "VYBAVENI" 
   (	"VYBAVENI_KEY" NUMBER(*,0), 
	"POBOCKA_KEY" NUMBER(*,0), 
	"CENA" VARCHAR2(20 CHAR), 
	"VYBAVENI_TYPE" VARCHAR2(8 BYTE)
   ) ;
--------------------------------------------------------
--  DDL for Table ZAMESTNANEC
--------------------------------------------------------

  CREATE TABLE "ZAMESTNANEC" 
   (	"ZAMESTNANEC_KEY" NUMBER(*,0), 
	"POBOCKA_KEY" NUMBER(*,0), 
	"ZAMESTNANEC_KEY_NADRIZENY" NUMBER(*,0), 
	"JMENO" VARCHAR2(50 CHAR), 
	"PRIJMENI" VARCHAR2(50 CHAR), 
	"CISLO_UCTU" VARCHAR2(50 CHAR), 
	"PLAT" VARCHAR2(20 CHAR), 
	"RODNE_CISLO" VARCHAR2(20 CHAR), 
	"TELEFON" VARCHAR2(20 CHAR)
   ) ;
--------------------------------------------------------
--  DDL for Table ZBOZI
--------------------------------------------------------

  CREATE TABLE "ZBOZI" 
   (	"ZBOZI_KEY" NUMBER(*,0), 
	"CENA" VARCHAR2(20 BYTE)
   ) ;
--------------------------------------------------------
--  DDL for Index ZBOZI_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "ZBOZI_PK" ON "ZBOZI" ("ZBOZI_KEY") 
  ;
--------------------------------------------------------
--  DDL for Index ZAMESTNANEC_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "ZAMESTNANEC_PK" ON "ZAMESTNANEC" ("ZAMESTNANEC_KEY") 
  ;
--------------------------------------------------------
--  DDL for Index VYBAVENI_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "VYBAVENI_PK" ON "VYBAVENI" ("VYBAVENI_KEY", "POBOCKA_KEY") 
  ;
--------------------------------------------------------
--  DDL for Index VODNI_DYMKA_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "VODNI_DYMKA_PK" ON "VODNI_DYMKA" ("VODNI_DYMKA_KEY") 
  ;
--------------------------------------------------------
--  DDL for Index STUL_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "STUL_PK" ON "STUL" ("POBOCKA_KEY", "CISLO") 
  ;
--------------------------------------------------------
--  DDL for Index REZERVACE_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "REZERVACE_PK" ON "REZERVACE" ("REZERVACE_KEY") 
  ;
--------------------------------------------------------
--  DDL for Index PREDMETY_DYMKA_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "PREDMETY_DYMKA_PK" ON "PREDMETY_DYMKA" ("PREDMETY_DYMKA_KEY") 
  ;
--------------------------------------------------------
--  DDL for Index PREDMETY_CAJ_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "PREDMETY_CAJ_PK" ON "PREDMETY_CAJ" ("PREDMETY_KEY") 
  ;
--------------------------------------------------------
--  DDL for Index POBOCKA_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "POBOCKA_PK" ON "POBOCKA" ("POBOCKA_KEY") 
  ;
--------------------------------------------------------
--  DDL for Index OBJEDNAVKY_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "OBJEDNAVKY_PK" ON "OBJEDNAVKY" ("POBOCKA_KEY", "CISLO", "OBJEDNAVKA_KEY") 
  ;
--------------------------------------------------------
--  DDL for Index OBJEDNAVKA_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "OBJEDNAVKA_PK" ON "OBJEDNAVKA" ("OBJEDNAVKA_KEY") 
  ;
--------------------------------------------------------
--  DDL for Index OBCERSTVENI_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "OBCERSTVENI_PK" ON "OBCERSTVENI" ("OBCERSTVENI_KEY") 
  ;
--------------------------------------------------------
--  DDL for Index OBCERSTVENI_NAZEV_UN
--------------------------------------------------------

  CREATE UNIQUE INDEX "OBCERSTVENI_NAZEV_UN" ON "OBCERSTVENI" ("NAZEV") 
  ;
--------------------------------------------------------
--  DDL for Index NADOBI_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "NADOBI_PK" ON "NADOBI" ("VYBAVENI_KEY", "POBOCKA_KEY") 
  ;
--------------------------------------------------------
--  DDL for Index NABYTEK_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "NABYTEK_PK" ON "NABYTEK" ("VYBAVENI_KEY", "POBOCKA_KEY") 
  ;
--------------------------------------------------------
--  DDL for Index JEDNOTLIVE_PORCE_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "JEDNOTLIVE_PORCE_PK" ON "JEDNOTLIVE_PORCE" ("CAJ_KEY", "JEDNOTLIVE_PORCE_KEY") 
  ;
--------------------------------------------------------
--  DDL for Index JEDNOTLIVE_KUSY_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "JEDNOTLIVE_KUSY_PK" ON "JEDNOTLIVE_KUSY" ("JEDNOTLIVE_KUSY_KEY", "OBCERSTVENI_KEY") 
  ;
--------------------------------------------------------
--  DDL for Index CAJ_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "CAJ_PK" ON "CAJ" ("CAJ_KEY") 
  ;
--------------------------------------------------------
--  DDL for Index CAJ_NAZEV_UN
--------------------------------------------------------

  CREATE UNIQUE INDEX "CAJ_NAZEV_UN" ON "CAJ" ("NAZEV") 
  ;
--------------------------------------------------------
--  DDL for Index ADRESA_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "ADRESA_PK" ON "ADRESA" ("ADRESA_KEY") 
  ;
--------------------------------------------------------
--  DDL for Index ADRESA__IDX
--------------------------------------------------------

  CREATE UNIQUE INDEX "ADRESA__IDX" ON "ADRESA" ("POBOCKA_KEY") 
  ;
--------------------------------------------------------
--  DDL for Trigger ARC_FKARC_1_NABYTEK
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "ARC_FKARC_1_NABYTEK" BEFORE
    INSERT OR UPDATE OF vybaveni_key,pobocka_key ON nabytek
    FOR EACH ROW
DECLARE
    d   VARCHAR2(8);
BEGIN
    SELECT
        a.vybaveni_type
    INTO
        d
    FROM
        vybaveni a
    WHERE
        a.vybaveni_key =:new.vybaveni_key
        AND   a.pobocka_key =:new.pobocka_key;

    IF
        ( d IS NULL OR d <> 'Nabytek' )
    THEN
        raise_application_error(-20223,'FK Nabytek_Vybaveni_FK in Table Nabytek violates Arc constraint on Table Vybaveni - discriminator column Vybaveni_TYPE doesn''t have value ''Nabytek'''
);
    END IF;

EXCEPTION
    WHEN no_data_found THEN
        NULL;
    WHEN OTHERS THEN
        RAISE;
END;

/
ALTER TRIGGER "ARC_FKARC_1_NABYTEK" ENABLE;
--------------------------------------------------------
--  DDL for Trigger ARC_FKARC_1_NADOBI
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "ARC_FKARC_1_NADOBI" BEFORE
    INSERT OR UPDATE OF vybaveni_key,pobocka_key ON nadobi
    FOR EACH ROW
DECLARE
    d   VARCHAR2(8);
BEGIN
    SELECT
        a.vybaveni_type
    INTO
        d
    FROM
        vybaveni a
    WHERE
        a.vybaveni_key =:new.vybaveni_key
        AND   a.pobocka_key =:new.pobocka_key;

    IF
        ( d IS NULL OR d <> 'Nadobi' )
    THEN
        raise_application_error(-20223,'FK Nadobi_Vybaveni_FK in Table Nadobi violates Arc constraint on Table Vybaveni - discriminator column Vybaveni_TYPE doesn''t have value ''Nadobi'''
);
    END IF;

EXCEPTION
    WHEN no_data_found THEN
        NULL;
    WHEN OTHERS THEN
        RAISE;
END;

/
ALTER TRIGGER "ARC_FKARC_1_NADOBI" ENABLE;
--------------------------------------------------------
--  DDL for Procedure SMAZ_VSECHNY_TABULKY
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "SMAZ_VSECHNY_TABULKY" AS
-- pokud v logu bude uvedeno, ze nektery objekt nebyl zrusen, protoze na nej jiny jeste existujici objekt stavi, spust proceduru opakovane, dokud se nezrusi vse
begin
  for iRec in 
    (select distinct OBJECT_TYPE, OBJECT_NAME,
      'drop '||OBJECT_TYPE||' "'||OBJECT_NAME||'"'||
      case OBJECT_TYPE when 'TABLE' then ' cascade constraints purge' else ' ' end as PRIKAZ
    from USER_OBJECTS where OBJECT_NAME not in ('SMAZ_VSECHNY_TABULKY', 'VYPNI_CIZI_KLICE', 'ZAPNI_CIZI_KLICE', 'VYMAZ_DATA_VSECH_TABULEK')
    ) loop
        begin
          dbms_output.put_line('Prikaz: '||irec.prikaz);
        execute immediate iRec.prikaz;
        exception
          when others then dbms_output.put_line('NEPOVEDLO SE!');
        end;
      end loop;
end;

/
--------------------------------------------------------
--  DDL for Procedure VYMAZ_DATA_VSECH_TABULEK
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "VYMAZ_DATA_VSECH_TABULEK" is
begin
  -- Vymazat data vsech tabulek
  VYPNI_CIZI_KLICE;
  for v_rec in (select distinct TABLE_NAME from USER_TABLES)
  loop
    execute immediate 'truncate table '||v_rec.TABLE_NAME||' drop storage';
  end loop;
  ZAPNI_CIZI_KLICE;

  -- Nastavit vsechny sekvence od 1
  for v_rec in (select distinct SEQUENCE_NAME  from USER_SEQUENCES)
  loop
    execute immediate 'alter sequence '||v_rec.SEQUENCE_NAME||' restart start with 1';
  end loop;
end VYMAZ_DATA_VSECH_TABULEK;

/
--------------------------------------------------------
--  DDL for Procedure VYPNI_CIZI_KLICE
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "VYPNI_CIZI_KLICE" as 
begin
  for cur in (select CONSTRAINT_NAME, TABLE_NAME from USER_CONSTRAINTS where CONSTRAINT_TYPE = 'R' ) 
  loop
    execute immediate 'alter table '||cur.TABLE_NAME||' modify constraint "'||cur.CONSTRAINT_NAME||'" DISABLE';
  end loop;
end VYPNI_CIZI_KLICE;

/
--------------------------------------------------------
--  DDL for Procedure ZAPNI_CIZI_KLICE
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "ZAPNI_CIZI_KLICE" as 
begin
  for cur in (select CONSTRAINT_NAME, TABLE_NAME from USER_CONSTRAINTS where CONSTRAINT_TYPE = 'R' ) 
  loop
    execute immediate 'alter table '||cur.TABLE_NAME||' modify constraint "'||cur.CONSTRAINT_NAME||'" enable validate';
  end loop;
end ZAPNI_CIZI_KLICE;

/
--------------------------------------------------------
--  Constraints for Table JEDNOTLIVE_PORCE
--------------------------------------------------------

  ALTER TABLE "JEDNOTLIVE_PORCE" ADD CONSTRAINT "ARC_5" CHECK (
        (
            ( zbozi_key IS NOT NULL )
            AND ( pobocka_key IS NULL )
            AND ( objednavka_key IS NULL )
        )
        OR (
            ( pobocka_key IS NOT NULL )
            AND ( zbozi_key IS NULL )
            AND ( objednavka_key IS NULL )
        )
        OR (
            ( objednavka_key IS NOT NULL )
            AND ( zbozi_key IS NULL )
            AND ( pobocka_key IS NULL )
        )
        OR (
            ( zbozi_key IS NULL )
            AND ( pobocka_key IS NULL )
            AND ( objednavka_key IS NULL )
        )
    ) ENABLE;
  ALTER TABLE "JEDNOTLIVE_PORCE" ADD CONSTRAINT "JEDNOTLIVE_PORCE_PK" PRIMARY KEY ("CAJ_KEY", "JEDNOTLIVE_PORCE_KEY")
  USING INDEX  ENABLE;
  ALTER TABLE "JEDNOTLIVE_PORCE" MODIFY ("JEDNOTLIVE_PORCE_KEY" NOT NULL ENABLE);
  ALTER TABLE "JEDNOTLIVE_PORCE" MODIFY ("CAJ_KEY" NOT NULL ENABLE);
  ALTER TABLE "JEDNOTLIVE_PORCE" MODIFY ("GRAMAZ" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table ZAMESTNANEC
--------------------------------------------------------

  ALTER TABLE "ZAMESTNANEC" MODIFY ("ZAMESTNANEC_KEY" NOT NULL ENABLE);
  ALTER TABLE "ZAMESTNANEC" MODIFY ("POBOCKA_KEY" NOT NULL ENABLE);
  ALTER TABLE "ZAMESTNANEC" MODIFY ("JMENO" NOT NULL ENABLE);
  ALTER TABLE "ZAMESTNANEC" MODIFY ("PRIJMENI" NOT NULL ENABLE);
  ALTER TABLE "ZAMESTNANEC" ADD CONSTRAINT "ZAMESTNANEC_PK" PRIMARY KEY ("ZAMESTNANEC_KEY")
  USING INDEX  ENABLE;
--------------------------------------------------------
--  Constraints for Table PREDMETY_CAJ
--------------------------------------------------------

  ALTER TABLE "PREDMETY_CAJ" ADD CONSTRAINT "PREDMETY_CAJ_PK" PRIMARY KEY ("PREDMETY_KEY")
  USING INDEX  ENABLE;
  ALTER TABLE "PREDMETY_CAJ" MODIFY ("PREDMETY_KEY" NOT NULL ENABLE);
  ALTER TABLE "PREDMETY_CAJ" MODIFY ("NAZEV" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table NADOBI
--------------------------------------------------------

  ALTER TABLE "NADOBI" ADD CONSTRAINT "NADOBI_PK" PRIMARY KEY ("VYBAVENI_KEY", "POBOCKA_KEY")
  USING INDEX  ENABLE;
  ALTER TABLE "NADOBI" MODIFY ("VYBAVENI_KEY" NOT NULL ENABLE);
  ALTER TABLE "NADOBI" MODIFY ("POBOCKA_KEY" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table CAJ
--------------------------------------------------------

  ALTER TABLE "CAJ" ADD CONSTRAINT "CAJ_NAZEV_UN" UNIQUE ("NAZEV")
  USING INDEX  ENABLE;
  ALTER TABLE "CAJ" ADD CONSTRAINT "CAJ_PK" PRIMARY KEY ("CAJ_KEY")
  USING INDEX  ENABLE;
  ALTER TABLE "CAJ" MODIFY ("CAJ_KEY" NOT NULL ENABLE);
  ALTER TABLE "CAJ" MODIFY ("NAZEV" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table ADRESA
--------------------------------------------------------

  ALTER TABLE "ADRESA" ADD CONSTRAINT "ADRESA_PK" PRIMARY KEY ("ADRESA_KEY")
  USING INDEX  ENABLE;
  ALTER TABLE "ADRESA" MODIFY ("ADRESA_KEY" NOT NULL ENABLE);
  ALTER TABLE "ADRESA" MODIFY ("POBOCKA_KEY" NOT NULL ENABLE);
  ALTER TABLE "ADRESA" MODIFY ("MESTO" NOT NULL ENABLE);
  ALTER TABLE "ADRESA" MODIFY ("POPISNE_CISLO" NOT NULL ENABLE);
  ALTER TABLE "ADRESA" MODIFY ("ULICE" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table POBOCKA
--------------------------------------------------------

  ALTER TABLE "POBOCKA" ADD CONSTRAINT "POBOCKA_PK" PRIMARY KEY ("POBOCKA_KEY")
  USING INDEX  ENABLE;
  ALTER TABLE "POBOCKA" MODIFY ("POBOCKA_KEY" NOT NULL ENABLE);
  ALTER TABLE "POBOCKA" MODIFY ("EMAIL" NOT NULL ENABLE);
  ALTER TABLE "POBOCKA" MODIFY ("NAZEV" NOT NULL ENABLE);
  ALTER TABLE "POBOCKA" MODIFY ("TELEFONNI_CISLO" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table ZBOZI
--------------------------------------------------------

  ALTER TABLE "ZBOZI" MODIFY ("ZBOZI_KEY" NOT NULL ENABLE);
  ALTER TABLE "ZBOZI" MODIFY ("CENA" NOT NULL ENABLE);
  ALTER TABLE "ZBOZI" ADD CONSTRAINT "ZBOZI_PK" PRIMARY KEY ("ZBOZI_KEY")
  USING INDEX  ENABLE;
--------------------------------------------------------
--  Constraints for Table OBJEDNAVKY
--------------------------------------------------------

  ALTER TABLE "OBJEDNAVKY" ADD CONSTRAINT "OBJEDNAVKY_PK" PRIMARY KEY ("POBOCKA_KEY", "CISLO", "OBJEDNAVKA_KEY")
  USING INDEX  ENABLE;
  ALTER TABLE "OBJEDNAVKY" MODIFY ("POBOCKA_KEY" NOT NULL ENABLE);
  ALTER TABLE "OBJEDNAVKY" MODIFY ("CISLO" NOT NULL ENABLE);
  ALTER TABLE "OBJEDNAVKY" MODIFY ("OBJEDNAVKA_KEY" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table PREDMETY_DYMKA
--------------------------------------------------------

  ALTER TABLE "PREDMETY_DYMKA" ADD CONSTRAINT "PREDMETY_DYMKA_PK" PRIMARY KEY ("PREDMETY_DYMKA_KEY")
  USING INDEX  ENABLE;
  ALTER TABLE "PREDMETY_DYMKA" MODIFY ("PREDMETY_DYMKA_KEY" NOT NULL ENABLE);
  ALTER TABLE "PREDMETY_DYMKA" MODIFY ("NAZEV" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table VYBAVENI
--------------------------------------------------------

  ALTER TABLE "VYBAVENI" ADD CONSTRAINT "CH_INH_VYBAVENI" CHECK ( vybaveni_type IN (
        'Nabytek',
        'Nadobi',
        'Vybaveni'
    ) ) ENABLE;
  ALTER TABLE "VYBAVENI" MODIFY ("VYBAVENI_KEY" NOT NULL ENABLE);
  ALTER TABLE "VYBAVENI" MODIFY ("POBOCKA_KEY" NOT NULL ENABLE);
  ALTER TABLE "VYBAVENI" MODIFY ("CENA" NOT NULL ENABLE);
  ALTER TABLE "VYBAVENI" MODIFY ("VYBAVENI_TYPE" NOT NULL ENABLE);
  ALTER TABLE "VYBAVENI" ADD CONSTRAINT "VYBAVENI_PK" PRIMARY KEY ("VYBAVENI_KEY", "POBOCKA_KEY")
  USING INDEX  ENABLE;
--------------------------------------------------------
--  Constraints for Table NABYTEK
--------------------------------------------------------

  ALTER TABLE "NABYTEK" ADD CONSTRAINT "NABYTEK_PK" PRIMARY KEY ("VYBAVENI_KEY", "POBOCKA_KEY")
  USING INDEX  ENABLE;
  ALTER TABLE "NABYTEK" MODIFY ("VYBAVENI_KEY" NOT NULL ENABLE);
  ALTER TABLE "NABYTEK" MODIFY ("POBOCKA_KEY" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table JEDNOTLIVE_KUSY
--------------------------------------------------------

  ALTER TABLE "JEDNOTLIVE_KUSY" ADD CONSTRAINT "ARC_6" CHECK (
        (
            ( objednavka_key IS NOT NULL )
            AND ( pobocka_key IS NULL )
            AND ( zbozi_key IS NULL )
        )
        OR (
            ( pobocka_key IS NOT NULL )
            AND ( objednavka_key IS NULL )
            AND ( zbozi_key IS NULL )
        )
        OR (
            ( zbozi_key IS NOT NULL )
            AND ( objednavka_key IS NULL )
            AND ( pobocka_key IS NULL )
        )
        OR (
            ( objednavka_key IS NULL )
            AND ( pobocka_key IS NULL )
            AND ( zbozi_key IS NULL )
        )
    ) ENABLE;
  ALTER TABLE "JEDNOTLIVE_KUSY" ADD CONSTRAINT "JEDNOTLIVE_KUSY_PK" PRIMARY KEY ("JEDNOTLIVE_KUSY_KEY", "OBCERSTVENI_KEY")
  USING INDEX  ENABLE;
  ALTER TABLE "JEDNOTLIVE_KUSY" MODIFY ("JEDNOTLIVE_KUSY_KEY" NOT NULL ENABLE);
  ALTER TABLE "JEDNOTLIVE_KUSY" MODIFY ("OBCERSTVENI_KEY" NOT NULL ENABLE);
  ALTER TABLE "JEDNOTLIVE_KUSY" MODIFY ("GRAMAZ" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table STUL
--------------------------------------------------------

  ALTER TABLE "STUL" ADD CONSTRAINT "STUL_PK" PRIMARY KEY ("POBOCKA_KEY", "CISLO")
  USING INDEX  ENABLE;
  ALTER TABLE "STUL" MODIFY ("CISLO" NOT NULL ENABLE);
  ALTER TABLE "STUL" MODIFY ("POBOCKA_KEY" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table VODNI_DYMKA
--------------------------------------------------------

  ALTER TABLE "VODNI_DYMKA" ADD CONSTRAINT "ARC_4" CHECK (
        (
            ( zbozi_key IS NOT NULL )
            AND ( objednavka_key IS NULL )
            AND ( pobocka_key IS NULL )
        )
        OR (
            ( objednavka_key IS NOT NULL )
            AND ( zbozi_key IS NULL )
            AND ( pobocka_key IS NULL )
        )
        OR (
            ( pobocka_key IS NOT NULL )
            AND ( zbozi_key IS NULL )
            AND ( objednavka_key IS NULL )
        )
        OR (
            ( zbozi_key IS NULL )
            AND ( objednavka_key IS NULL )
            AND ( pobocka_key IS NULL )
        )
    ) ENABLE;
  ALTER TABLE "VODNI_DYMKA" MODIFY ("VODNI_DYMKA_KEY" NOT NULL ENABLE);
  ALTER TABLE "VODNI_DYMKA" MODIFY ("TYP" NOT NULL ENABLE);
  ALTER TABLE "VODNI_DYMKA" MODIFY ("VYSKA" NOT NULL ENABLE);
  ALTER TABLE "VODNI_DYMKA" ADD CONSTRAINT "VODNI_DYMKA_PK" PRIMARY KEY ("VODNI_DYMKA_KEY")
  USING INDEX  ENABLE;
--------------------------------------------------------
--  Constraints for Table OBCERSTVENI
--------------------------------------------------------

  ALTER TABLE "OBCERSTVENI" ADD CONSTRAINT "OBCERSTVENI_NAZEV_UN" UNIQUE ("NAZEV")
  USING INDEX  ENABLE;
  ALTER TABLE "OBCERSTVENI" ADD CONSTRAINT "OBCERSTVENI_PK" PRIMARY KEY ("OBCERSTVENI_KEY")
  USING INDEX  ENABLE;
  ALTER TABLE "OBCERSTVENI" MODIFY ("OBCERSTVENI_KEY" NOT NULL ENABLE);
  ALTER TABLE "OBCERSTVENI" MODIFY ("NAZEV" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table REZERVACE
--------------------------------------------------------

  ALTER TABLE "REZERVACE" ADD CONSTRAINT "REZERVACE_PK" PRIMARY KEY ("REZERVACE_KEY")
  USING INDEX  ENABLE;
  ALTER TABLE "REZERVACE" MODIFY ("REZERVACE_KEY" NOT NULL ENABLE);
  ALTER TABLE "REZERVACE" MODIFY ("POBOCKA_KEY" NOT NULL ENABLE);
  ALTER TABLE "REZERVACE" MODIFY ("CISLO" NOT NULL ENABLE);
  ALTER TABLE "REZERVACE" MODIFY ("CAS" NOT NULL ENABLE);
  ALTER TABLE "REZERVACE" MODIFY ("DATUM" NOT NULL ENABLE);
  ALTER TABLE "REZERVACE" MODIFY ("PRIJMENI" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table OBJEDNAVKA
--------------------------------------------------------

  ALTER TABLE "OBJEDNAVKA" ADD CONSTRAINT "OBJEDNAVKA_PK" PRIMARY KEY ("OBJEDNAVKA_KEY")
  USING INDEX  ENABLE;
  ALTER TABLE "OBJEDNAVKA" MODIFY ("OBJEDNAVKA_KEY" NOT NULL ENABLE);
--------------------------------------------------------
--  Ref Constraints for Table ADRESA
--------------------------------------------------------

  ALTER TABLE "ADRESA" ADD CONSTRAINT "ADRESA_POBOCKA_FK" FOREIGN KEY ("POBOCKA_KEY")
	  REFERENCES "POBOCKA" ("POBOCKA_KEY") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table JEDNOTLIVE_KUSY
--------------------------------------------------------

  ALTER TABLE "JEDNOTLIVE_KUSY" ADD CONSTRAINT "JEDNOTLIVE_KUSY_OBCERSTVENI_FK" FOREIGN KEY ("OBCERSTVENI_KEY")
	  REFERENCES "OBCERSTVENI" ("OBCERSTVENI_KEY") ENABLE;
  ALTER TABLE "JEDNOTLIVE_KUSY" ADD CONSTRAINT "JEDNOTLIVE_KUSY_OBJEDNAVKA_FK" FOREIGN KEY ("OBJEDNAVKA_KEY")
	  REFERENCES "OBJEDNAVKA" ("OBJEDNAVKA_KEY") ENABLE;
  ALTER TABLE "JEDNOTLIVE_KUSY" ADD CONSTRAINT "JEDNOTLIVE_KUSY_POBOCKA_FK" FOREIGN KEY ("POBOCKA_KEY")
	  REFERENCES "POBOCKA" ("POBOCKA_KEY") ENABLE;
  ALTER TABLE "JEDNOTLIVE_KUSY" ADD CONSTRAINT "JEDNOTLIVE_KUSY_ZBOZI_FK" FOREIGN KEY ("ZBOZI_KEY")
	  REFERENCES "ZBOZI" ("ZBOZI_KEY") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table JEDNOTLIVE_PORCE
--------------------------------------------------------

  ALTER TABLE "JEDNOTLIVE_PORCE" ADD CONSTRAINT "JEDNOTLIVE_PORCE_CAJ_FK" FOREIGN KEY ("CAJ_KEY")
	  REFERENCES "CAJ" ("CAJ_KEY") ENABLE;
  ALTER TABLE "JEDNOTLIVE_PORCE" ADD CONSTRAINT "JEDNOTLIVE_PORCE_OBJEDNAVKA_FK" FOREIGN KEY ("OBJEDNAVKA_KEY")
	  REFERENCES "OBJEDNAVKA" ("OBJEDNAVKA_KEY") ENABLE;
  ALTER TABLE "JEDNOTLIVE_PORCE" ADD CONSTRAINT "JEDNOTLIVE_PORCE_POBOCKA_FK" FOREIGN KEY ("POBOCKA_KEY")
	  REFERENCES "POBOCKA" ("POBOCKA_KEY") ENABLE;
  ALTER TABLE "JEDNOTLIVE_PORCE" ADD CONSTRAINT "JEDNOTLIVE_PORCE_ZBOZI_FK" FOREIGN KEY ("ZBOZI_KEY")
	  REFERENCES "ZBOZI" ("ZBOZI_KEY") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table NABYTEK
--------------------------------------------------------

  ALTER TABLE "NABYTEK" ADD CONSTRAINT "NABYTEK_VYBAVENI_FK" FOREIGN KEY ("VYBAVENI_KEY", "POBOCKA_KEY")
	  REFERENCES "VYBAVENI" ("VYBAVENI_KEY", "POBOCKA_KEY") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table NADOBI
--------------------------------------------------------

  ALTER TABLE "NADOBI" ADD CONSTRAINT "NADOBI_VYBAVENI_FK" FOREIGN KEY ("VYBAVENI_KEY", "POBOCKA_KEY")
	  REFERENCES "VYBAVENI" ("VYBAVENI_KEY", "POBOCKA_KEY") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table OBJEDNAVKY
--------------------------------------------------------

  ALTER TABLE "OBJEDNAVKY" ADD CONSTRAINT "OBJEDNAVKY_OBJEDNAVKA_FK" FOREIGN KEY ("OBJEDNAVKA_KEY")
	  REFERENCES "OBJEDNAVKA" ("OBJEDNAVKA_KEY") ENABLE;
  ALTER TABLE "OBJEDNAVKY" ADD CONSTRAINT "OBJEDNAVKY_STUL_FK" FOREIGN KEY ("POBOCKA_KEY", "CISLO")
	  REFERENCES "STUL" ("POBOCKA_KEY", "CISLO") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table PREDMETY_CAJ
--------------------------------------------------------

  ALTER TABLE "PREDMETY_CAJ" ADD CONSTRAINT "PREDMETY_CAJ_ZBOZI_FK" FOREIGN KEY ("ZBOZI_KEY")
	  REFERENCES "ZBOZI" ("ZBOZI_KEY") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table PREDMETY_DYMKA
--------------------------------------------------------

  ALTER TABLE "PREDMETY_DYMKA" ADD CONSTRAINT "PREDMETY_DYMKA_ZBOZI_FK" FOREIGN KEY ("ZBOZI_KEY")
	  REFERENCES "ZBOZI" ("ZBOZI_KEY") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table REZERVACE
--------------------------------------------------------

  ALTER TABLE "REZERVACE" ADD CONSTRAINT "REZERVACE_STUL_FK" FOREIGN KEY ("POBOCKA_KEY", "CISLO")
	  REFERENCES "STUL" ("POBOCKA_KEY", "CISLO") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table STUL
--------------------------------------------------------

  ALTER TABLE "STUL" ADD CONSTRAINT "STUL_POBOCKA_FK" FOREIGN KEY ("POBOCKA_KEY")
	  REFERENCES "POBOCKA" ("POBOCKA_KEY") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table VODNI_DYMKA
--------------------------------------------------------

  ALTER TABLE "VODNI_DYMKA" ADD CONSTRAINT "VODNI_DYMKA_OBJEDNAVKA_FK" FOREIGN KEY ("OBJEDNAVKA_KEY")
	  REFERENCES "OBJEDNAVKA" ("OBJEDNAVKA_KEY") ENABLE;
  ALTER TABLE "VODNI_DYMKA" ADD CONSTRAINT "VODNI_DYMKA_POBOCKA_FK" FOREIGN KEY ("POBOCKA_KEY")
	  REFERENCES "POBOCKA" ("POBOCKA_KEY") ENABLE;
  ALTER TABLE "VODNI_DYMKA" ADD CONSTRAINT "VODNI_DYMKA_ZBOZI_FK" FOREIGN KEY ("ZBOZI_KEY")
	  REFERENCES "ZBOZI" ("ZBOZI_KEY") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table VYBAVENI
--------------------------------------------------------

  ALTER TABLE "VYBAVENI" ADD CONSTRAINT "VYBAVENI_POBOCKA_FK" FOREIGN KEY ("POBOCKA_KEY")
	  REFERENCES "POBOCKA" ("POBOCKA_KEY") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table ZAMESTNANEC
--------------------------------------------------------

  ALTER TABLE "ZAMESTNANEC" ADD CONSTRAINT "ZAMESTNANEC_POBOCKA_FK" FOREIGN KEY ("POBOCKA_KEY")
	  REFERENCES "POBOCKA" ("POBOCKA_KEY") ENABLE;
  ALTER TABLE "ZAMESTNANEC" ADD CONSTRAINT "ZAMESTNANEC_ZAMESTNANEC_FK" FOREIGN KEY ("ZAMESTNANEC_KEY_NADRIZENY")
	  REFERENCES "ZAMESTNANEC" ("ZAMESTNANEC_KEY") ENABLE;
