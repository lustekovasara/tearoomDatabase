prompt #-----------------------#
prompt #- Smazat vsechna data -#
prompt #-----------------------#

exec VYMAZ_DATA_VSECH_TABULEK;

prompt #----------------------#
prompt #- Vypnout cizi klice -#
prompt #----------------------#

exec VYPNI_CIZI_KLICE;

prompt #---------------#
prompt #- Vlozit data -#
prompt #---------------#



INSERT INTO Pobocka (pobocka_key, email, nazev, telefonni_cislo, webova_adresa) VALUES ('1', 'AlioseHavla@podivnecajoveritualy.cz', 'Podivné čajové rituály - Aloise Havla', '+420 326 610 420', 'www.pcraloisehavla.cz');
INSERT INTO Pobocka (pobocka_key, email, nazev, telefonni_cislo, webova_adresa) VALUES ('2', 'Ecerova@podivnecajoveritualy.cz', 'Podivné čajové rituály - Ečerova', '+420 517 549 657', 'www.pcrecerova.cz');
INSERT INTO Pobocka (pobocka_key, email, nazev, telefonni_cislo, webova_adresa) VALUES ('3', 'Nohavicova@podivnecajoveritualy.cz', 'Podivné čajové rituály - Nohavicova', '+420 382 854 496', 'www.pcrnohavicova.cz');
INSERT INTO Pobocka (pobocka_key, email, nazev, telefonni_cislo, webova_adresa) VALUES ('4', 'Zahradni@podivnecajoveritualy.cz', 'Podivné čajové rituály - Zahradní', '+420 379 769 242', 'www.pcrzahradni.cz');
INSERT INTO Pobocka (pobocka_key, email, nazev, telefonni_cislo, webova_adresa) VALUES ('5', 'Sobotkova@podivnecajoveritualy.cz', 'Podivné čajové rituály - Sobotkova', '+420 495 477 015', 'www.pcrsobotkova.cz');


INSERT INTO Adresa (adresa_key, pobocka_key, mesto, popisne_cislo, ulice, psc) VALUES ('1', '1', 'Strakonice', '49', 'Aloise Havla', '386 01');
INSERT INTO Adresa (adresa_key, pobocka_key, mesto, popisne_cislo, ulice, psc) VALUES ('2', '2', 'Frýdek-Místek', '89', 'Ečerova', '739 51');
INSERT INTO Adresa (adresa_key, pobocka_key, mesto, popisne_cislo, ulice, psc) VALUES ('3', '3', 'Prostějov', '42', 'Nohavicova', '796 07');
INSERT INTO Adresa (adresa_key, pobocka_key, mesto, popisne_cislo, ulice, psc) VALUES ('4', '4', 'Žďár nad Sázavou', '15', 'Zahradní', '591 02');
INSERT INTO Adresa (adresa_key, pobocka_key, mesto, popisne_cislo, ulice, psc) VALUES ('5', '5', 'Třebíč', '71', 'Sobotkova', '674 03');



INSERT INTO stul (cislo, pobocka_key) VALUES ('1', '1');
INSERT INTO stul (cislo, pobocka_key) VALUES ('2', '1');
INSERT INTO stul (cislo, pobocka_key) VALUES ('3', '1');
INSERT INTO stul (cislo, pobocka_key) VALUES ('4', '1');
INSERT INTO stul (cislo, pobocka_key) VALUES ('5', '1');
INSERT INTO stul (cislo, pobocka_key) VALUES ('6', '1');
INSERT INTO stul (cislo, pobocka_key) VALUES ('7', '1');
INSERT INTO stul (cislo, pobocka_key) VALUES ('8', '1');
INSERT INTO stul (cislo, pobocka_key) VALUES ('9', '1');
INSERT INTO stul (cislo, pobocka_key) VALUES ('10', '1');

INSERT INTO stul (cislo, pobocka_key) VALUES ('1', '2');
INSERT INTO stul (cislo, pobocka_key) VALUES ('2', '2');
INSERT INTO stul (cislo, pobocka_key) VALUES ('3', '2');
INSERT INTO stul (cislo, pobocka_key) VALUES ('4', '2');
INSERT INTO stul (cislo, pobocka_key) VALUES ('5', '2');
INSERT INTO stul (cislo, pobocka_key) VALUES ('6', '2');
INSERT INTO stul (cislo, pobocka_key) VALUES ('7', '2');
INSERT INTO stul (cislo, pobocka_key) VALUES ('8', '2');
INSERT INTO stul (cislo, pobocka_key) VALUES ('9', '2');
INSERT INTO stul (cislo, pobocka_key) VALUES ('10', '2');

INSERT INTO stul (cislo, pobocka_key) VALUES ('1', '3');
INSERT INTO stul (cislo, pobocka_key) VALUES ('2', '3');
INSERT INTO stul (cislo, pobocka_key) VALUES ('3', '3');
INSERT INTO stul (cislo, pobocka_key) VALUES ('4', '3');
INSERT INTO stul (cislo, pobocka_key) VALUES ('5', '3');
INSERT INTO stul (cislo, pobocka_key) VALUES ('6', '3');
INSERT INTO stul (cislo, pobocka_key) VALUES ('7', '3');
INSERT INTO stul (cislo, pobocka_key) VALUES ('8', '3');
INSERT INTO stul (cislo, pobocka_key) VALUES ('9', '3');
INSERT INTO stul (cislo, pobocka_key) VALUES ('10', '3');


INSERT INTO stul (cislo, pobocka_key) VALUES ('1', '4');
INSERT INTO stul (cislo, pobocka_key) VALUES ('2', '4');
INSERT INTO stul (cislo, pobocka_key) VALUES ('3', '4');
INSERT INTO stul (cislo, pobocka_key) VALUES ('4', '4');
INSERT INTO stul (cislo, pobocka_key) VALUES ('5', '4');
INSERT INTO stul (cislo, pobocka_key) VALUES ('6', '4');
INSERT INTO stul (cislo, pobocka_key) VALUES ('7', '4');
INSERT INTO stul (cislo, pobocka_key) VALUES ('8', '4');
INSERT INTO stul (cislo, pobocka_key) VALUES ('9', '4');
INSERT INTO stul (cislo, pobocka_key) VALUES ('10', '4');

INSERT INTO stul (cislo, pobocka_key) VALUES ('1', '5');
INSERT INTO stul (cislo, pobocka_key) VALUES ('2', '5');
INSERT INTO stul (cislo, pobocka_key) VALUES ('3', '5');
INSERT INTO stul (cislo, pobocka_key) VALUES ('4', '5');
INSERT INTO stul (cislo, pobocka_key) VALUES ('5', '5');
INSERT INTO stul (cislo, pobocka_key) VALUES ('6', '5');
INSERT INTO stul (cislo, pobocka_key) VALUES ('7', '5');
INSERT INTO stul (cislo, pobocka_key) VALUES ('8', '5');
INSERT INTO stul (cislo, pobocka_key) VALUES ('9', '5');
INSERT INTO stul (cislo, pobocka_key) VALUES ('10', '5');




INSERT INTO zamestnanec (Zamestnanec_key, pobocka_key, Zamestnanec_key_nadrizeny, jmeno, prijmeni, cislo_uctu, plat, rodne_cislo, telefon) VALUES ('2', '1', NULL, 'Ladislav', 'Kadlec', '1530231839', '9015', '7911094521', '725 852 611');
INSERT INTO zamestnanec (Zamestnanec_key, pobocka_key, Zamestnanec_key_nadrizeny, jmeno, prijmeni, cislo_uctu, plat, rodne_cislo, telefon) VALUES ('3', '2', NULL, 'Květa', 'Kantorová', '1385971600', '19238', '8652132549', '728 367 724');
INSERT INTO zamestnanec (Zamestnanec_key, pobocka_key, Zamestnanec_key_nadrizeny, jmeno, prijmeni, cislo_uctu, plat, rodne_cislo, telefon) VALUES ('4', '3', NULL, 'Simona', 'Rychtaříková', '1174169484', '11336', '9453124582', '606 914 429');
INSERT INTO zamestnanec (Zamestnanec_key, pobocka_key, Zamestnanec_key_nadrizeny, jmeno, prijmeni, cislo_uctu, plat, rodne_cislo, telefon) VALUES ('5', '4', NULL, 'Iveta', 'Valterová', '1033475853', '10356', '7856121578', '725 446 874');
INSERT INTO zamestnanec (Zamestnanec_key, pobocka_key, Zamestnanec_key_nadrizeny, jmeno, prijmeni, cislo_uctu, plat, rodne_cislo, telefon) VALUES ('6', '5', NULL, 'Jitka', 'Davidová', '967648758', '14476', '9551061586', '602 286 459');
INSERT INTO zamestnanec (Zamestnanec_key, pobocka_key, Zamestnanec_key_nadrizeny, jmeno, prijmeni, cislo_uctu, plat, rodne_cislo, telefon) VALUES ('10', '3', '4', 'Gustav', 'Pospěch', '1786509986', '18383', NULL, '726 137 956');
INSERT INTO zamestnanec (Zamestnanec_key, pobocka_key, Zamestnanec_key_nadrizeny, jmeno, prijmeni, cislo_uctu, plat, rodne_cislo, telefon) VALUES ('12', '1', '2', 'Zita', 'Závodná', '1681858995', '14943', '9452294692', '730 333 716');
INSERT INTO zamestnanec (Zamestnanec_key, pobocka_key, Zamestnanec_key_nadrizeny, jmeno, prijmeni, cislo_uctu, plat, rodne_cislo, telefon) VALUES ('14', '4', '5', 'Luboš', 'Nohejl', '947904558', '19593', '4896512358', '603 250 912');
INSERT INTO zamestnanec (Zamestnanec_key, pobocka_key, Zamestnanec_key_nadrizeny, jmeno, prijmeni, cislo_uctu, plat, rodne_cislo, telefon) VALUES ('20', '5', '6', 'Ondřej', 'Kačer', '1891849965', '13251', '4598153545', '734 124 930');
INSERT INTO zamestnanec (Zamestnanec_key, pobocka_key, Zamestnanec_key_nadrizeny, jmeno, prijmeni, cislo_uctu, plat, rodne_cislo, telefon) VALUES ('29', '4', '5', 'Julius', 'Krejčíř', '1643411471', '11362', '4589652635', '727 417 158');
INSERT INTO zamestnanec (Zamestnanec_key, pobocka_key, Zamestnanec_key_nadrizeny, jmeno, prijmeni, cislo_uctu, plat, rodne_cislo, telefon) VALUES ('34', '3', '4', 'Pravoslav', 'Loskot', '1809338678', '9670', '7805261258', '722 281 019');
INSERT INTO zamestnanec (Zamestnanec_key, pobocka_key, Zamestnanec_key_nadrizeny, jmeno, prijmeni, cislo_uctu, plat, rodne_cislo, telefon) VALUES ('36', '5', '6', 'Renáta', 'Weberová', '1455770273', '9552', '1598546499', '733 363 989');
INSERT INTO zamestnanec (Zamestnanec_key, pobocka_key, Zamestnanec_key_nadrizeny, jmeno, prijmeni, cislo_uctu, plat, rodne_cislo, telefon) VALUES ('42', '2', '3', 'Izabela', 'Husárová', '1509324719', '12847', '9354121598', '732 981 629');
INSERT INTO zamestnanec (Zamestnanec_key, pobocka_key, Zamestnanec_key_nadrizeny, jmeno, prijmeni, cislo_uctu, plat, rodne_cislo, telefon) VALUES ('45', '3', '4', 'Marek', 'Kráčmar', '1010559160', '7448', '1268964531', '602 204 619');
INSERT INTO zamestnanec (Zamestnanec_key, pobocka_key, Zamestnanec_key_nadrizeny, jmeno, prijmeni, cislo_uctu, plat, rodne_cislo, telefon) VALUES ('49', '3', '4', 'Antonín', 'Lacina', '1726890689', '16959', '9805361546', '602 310 555');
INSERT INTO zamestnanec (Zamestnanec_key, pobocka_key, Zamestnanec_key_nadrizeny, jmeno, prijmeni, cislo_uctu, plat, rodne_cislo, telefon) VALUES ('59', '4', '5', 'Simona', 'Stibůrková', '1600744657', '9335', NULL, '603 554 169');
INSERT INTO zamestnanec (Zamestnanec_key, pobocka_key, Zamestnanec_key_nadrizeny, jmeno, prijmeni, cislo_uctu, plat, rodne_cislo, telefon) VALUES ('65', '4', '5', 'Emil', 'Brůna', '1751385244', '11466', NULL, '720 782 884');
INSERT INTO zamestnanec (Zamestnanec_key, pobocka_key, Zamestnanec_key_nadrizeny, jmeno, prijmeni, cislo_uctu, plat, rodne_cislo, telefon) VALUES ('71', '3', '4', 'Marta', 'Hessová', '1812594574', '11862', NULL, '604 518 032');
INSERT INTO zamestnanec (Zamestnanec_key, pobocka_key, Zamestnanec_key_nadrizeny, jmeno, prijmeni, cislo_uctu, plat, rodne_cislo, telefon) VALUES ('75', '3', '75', 'Horymír', 'Plecháč', '997742675', '13965', '9805124689', '727 798 002');
INSERT INTO zamestnanec (Zamestnanec_key, pobocka_key, Zamestnanec_key_nadrizeny, jmeno, prijmeni, cislo_uctu, plat, rodne_cislo, telefon) VALUES ('79', '2', '3', 'Anděla', 'Valešová', '1206272099', '17534', '9855064896', '730 587 352');
INSERT INTO zamestnanec (Zamestnanec_key, pobocka_key, Zamestnanec_key_nadrizeny, jmeno, prijmeni, cislo_uctu, plat, rodne_cislo, telefon) VALUES ('84', '4', '84', 'Hanuš', 'Kožený', '1593842081', '7067', NULL, '729 778 804');
INSERT INTO zamestnanec (Zamestnanec_key, pobocka_key, Zamestnanec_key_nadrizeny, jmeno, prijmeni, cislo_uctu, plat, rodne_cislo, telefon) VALUES ('86', '2', '3', 'Světlana', 'Stibůrková', '1187487517', '13965', NULL, '777 462 055');
INSERT INTO zamestnanec (Zamestnanec_key, pobocka_key, Zamestnanec_key_nadrizeny, jmeno, prijmeni, cislo_uctu, plat, rodne_cislo, telefon) VALUES ('87', '4', '87', 'Jitka', 'Lhotová', '934947996', '8238', NULL, '726 103 339');
INSERT INTO zamestnanec (Zamestnanec_key, pobocka_key, Zamestnanec_key_nadrizeny, jmeno, prijmeni, cislo_uctu, plat, rodne_cislo, telefon) VALUES ('90', '1', '2', 'Vladislav', 'Kružík', '1136131087', '13468', '9606244568', '723 610 857');
INSERT INTO zamestnanec (Zamestnanec_key, pobocka_key, Zamestnanec_key_nadrizeny, jmeno, prijmeni, cislo_uctu, plat, rodne_cislo, telefon) VALUES ('92', '5', '6', 'Miloslava', 'Michalíková', '1853322528', '9212', NULL, '776 346 095');


INSERT INTO Obcerstveni (obcerstveni_key, nazev) VALUES ('1', 'Chalva');
INSERT INTO Obcerstveni (obcerstveni_key, nazev) VALUES ('2', 'Baklava');
INSERT INTO Obcerstveni (obcerstveni_key, nazev) VALUES ('3', 'Pita se skořicí');
INSERT INTO Obcerstveni (obcerstveni_key, nazev) VALUES ('4', 'Čajové sušenky');
INSERT INTO Obcerstveni (obcerstveni_key, nazev) VALUES ('5', 'Směs oříšků');

INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('111', '1', NULL, NULL, '1', '70');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('211', '1', NULL, NULL, '1', '70');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('311', '1', NULL, NULL, '1', '70');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('411', '1', NULL, NULL, '1', '70');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('511', '1', NULL, NULL, '1', '70');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('611', '1', NULL, NULL, '1', '70');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('711', '1', NULL, NULL, '1', '70');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('811', '1', NULL, NULL, '1', '70');



INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('121', '2', '3', NULL, NULL, '50');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('221', '2', NULL, NULL, '1', '50');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('321', '2', NULL, NULL, '1', '50');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('421', '2', NULL, NULL, '1', '50');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('521', '2', NULL, NULL, '1', '50');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('621', '2', NULL, NULL, '1', '50');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('721', '2', NULL, NULL, '1', '50');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('821', '2', NULL, NULL, '1', '50');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('921', '2', NULL, NULL, '1', '50');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('1021', '2', NULL, NULL, '1', '50');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('1121', '2', NULL, NULL, '1', '50');



INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('131', '3', '3', NULL, NULL, '150');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('231', '3', NULL, NULL, '1', '150');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('331', '3', NULL, NULL, '1', '150');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('431', '3', NULL, NULL, '1', '150');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('531', '3', NULL, NULL, '1', '150');


INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('141', '4', NULL, NULL, '1', '170');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('241', '4', NULL, NULL, '1', '170');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('341', '4', NULL, NULL, '1', '170');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('441', '4', NULL, NULL, '1', '170');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('541', '4', NULL, NULL, '1', '170');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('641', '4', NULL, NULL, '1', '170');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('741', '4', NULL, NULL, '1', '170');


INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('151', '5', NULL, NULL, '1', '120');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('251', '5', NULL, NULL, '1', '120');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('351', '5', NULL, NULL, '1', '120');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('451', '5', NULL, NULL, '1', '120');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('551', '5', NULL, NULL, '1', '120');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('651', '5', NULL, NULL, '1', '120');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('751', '5', NULL, NULL, '1', '120');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('851', '5', NULL, NULL, '1', '120');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('951', '5', NULL, NULL, '1', '120');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('1051', '5', NULL, NULL, '1', '120');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('1151', '5', NULL, NULL, '1', '120');




INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('112', '1', NULL, NULL, '2', '70');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('212', '1', NULL, NULL, '2', '70');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('312', '1', NULL, NULL, '2', '70');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('412', '1', NULL, NULL, '2', '70');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('512', '1', NULL, NULL, '2', '70');


INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('122', '2', NULL, '3', NULL, '50');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('222', '2', NULL, '3', NULL, '50');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('322', '2', NULL, NULL, '2', '50');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('422', '2', NULL, NULL, '2', '50');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('522', '2', NULL, NULL, '2', '50');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('622', '2', NULL, NULL, '2', '50');


INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('132', '3', NULL, NULL, '2', '150');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('232', '3', NULL, NULL, '2', '150');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('332', '3', NULL, NULL, '2', '150');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('432', '3', NULL, NULL, '2', '150');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('532', '3', NULL, NULL, '2', '150');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('632', '3', NULL, NULL, '2', '150');


INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('142', '4', NULL, NULL, '2', '170');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('242', '4', NULL, NULL, '2', '170');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('342', '4', NULL, NULL, '2', '170');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('442', '4', NULL, NULL, '2', '170');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('542', '4', NULL, NULL, '2', '170');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('642', '4', NULL, NULL, '2', '170');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('742', '4', NULL, NULL, '2', '170');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('842', '4', NULL, NULL, '2', '170');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('942', '4', NULL, NULL, '2', '170');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('1042', '4', NULL, NULL, '2', '170');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('1142', '4', NULL, NULL, '2', '170');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('1242', '4', NULL, NULL, '2', '170');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('1342', '4', NULL, NULL, '2', '170');


INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('152', '5', NULL, NULL, '2', '120');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('252', '5', NULL, NULL, '2', '120');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('352', '5', NULL, NULL, '2', '120');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('452', '5', NULL, NULL, '2', '120');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('552', '5', NULL, NULL, '2', '120');

INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('113', '1', NULL, NULL, '3', '70');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('213', '1', NULL, NULL, '3', '70');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('313', '1', NULL, NULL, '3', '70');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('413', '1', NULL, NULL, '3', '70');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('513', '1', NULL, NULL, '3', '70');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('613', '1', NULL, NULL, '3', '70');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('713', '1', NULL, NULL, '3', '70');

INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('123', '2', NULL, '2', NULL, '50');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('223', '2', NULL, NULL, '3', '50');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('323', '2', NULL, NULL, '3', '50');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('423', '2', NULL, NULL, '3', '50');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('523', '2', NULL, NULL, '3', '50');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('623', '2', NULL, NULL, '3', '50');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('723', '2', NULL, NULL, '3', '50');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('823', '2', NULL, NULL, '3', '50');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('923', '2', NULL, NULL, '3', '50');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('1023', '2', NULL, NULL, '3', '50');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('1123', '2', NULL, NULL, '3', '50');

INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('133', '3', NULL, '2', NULL, '150');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('233', '3', NULL, NULL, '3', '150');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('333', '3', NULL, NULL, '3', '150');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('433', '3', NULL, NULL, '3', '150');

INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('143', '4', '4', NULL, NULL, '170');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('243', '4', '4', NULL, NULL, '170');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('343', '4', NULL, NULL, '3', '170');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('443', '4', NULL, NULL, '3', '170');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('543', '4', NULL, NULL, '3', '170');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('643', '4', NULL, NULL, '3', '170');

INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('153', '5', NULL, NULL, '3', '120');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('253', '5', NULL, NULL, '3', '120');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('353', '5', NULL, NULL, '3', '120');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('453', '5', NULL, NULL, '3', '120');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('553', '5', NULL, NULL, '3', '120');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('653', '5', NULL, NULL, '3', '120');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('753', '5', NULL, NULL, '3', '120');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('853', '5', NULL, NULL, '3', '120');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('953', '5', NULL, NULL, '3', '120');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('1053', '5', NULL, NULL, '3', '120');


INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('114', '1', NULL, NULL, '4', '70');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('214', '1', NULL, NULL, '4', '70');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('314', '1', NULL, NULL, '4', '70');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('414', '1', NULL, NULL, '4', '70');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('514', '1', NULL, NULL, '4', '70');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('614', '1', NULL, NULL, '4', '70');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('714', '1', NULL, NULL, '4', '70');


INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('124', '2', '2', NULL, NULL, '50');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('224', '2', '2', NULL, NULL, '50');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('324', '2', NULL, NULL, '4', '50');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('424', '2', NULL, NULL, '4', '50');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('524', '2', NULL, NULL, '4', '50');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('624', '2', NULL, NULL, '4', '50');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('724', '2', NULL, NULL, '4', '50');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('824', '2', NULL, NULL, '4', '50');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('924', '2', NULL, NULL, '4', '50');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('1024', '2', NULL, NULL, '4', '50');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('1124', '2', NULL, NULL, '4', '50');


INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('134', '3', '2', NULL, NULL, '150');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('234', '3', NULL, NULL, '4', '150');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('334', '3', NULL, NULL, '4', '150');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('434', '3', NULL, NULL, '4', '150');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('534', '3', NULL, NULL, '4', '150');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('634', '3', NULL, NULL, '4', '150');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('734', '3', NULL, NULL, '4', '150');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('834', '3', NULL, NULL, '4', '150');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('934', '3', NULL, NULL, '4', '150');


INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('144', '4', NULL, NULL, '4', '170');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('244', '4', NULL, NULL, '4', '170');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('344', '4', NULL, NULL, '4', '170');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('444', '4', NULL, NULL, '4', '170');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('544', '4', NULL, NULL, '4', '170');


INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('154', '5', NULL, NULL, '4', '120');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('254', '5', NULL, NULL, '4', '120');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('354', '5', NULL, NULL, '4', '120');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('454', '5', NULL, NULL, '4', '120');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('554', '5', NULL, NULL, '4', '120');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('654', '5', NULL, NULL, '4', '120');





INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('115', '1', NULL, NULL, '5', '70');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('215', '1', NULL, NULL, '5', '70');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('315', '1', NULL, NULL, '5', '70');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('415', '1', NULL, NULL, '5', '70');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('515', '1', NULL, NULL, '5', '70');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('615', '1', NULL, NULL, '5', '70');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('715', '1', NULL, NULL, '5', '70');


INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('125', '2', NULL, NULL, '5', '50');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('225', '2', NULL, NULL, '5', '50');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('325', '2', NULL, NULL, '5', '50');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('425', '2', NULL, NULL, '5', '50');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('525', '2', NULL, NULL, '5', '50');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('625', '2', NULL, NULL, '5', '50');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('725', '2', NULL, NULL, '5', '50');


INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('135', '3', NULL, NULL, '5', '150');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('235', '3', NULL, NULL, '5', '150');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('335', '3', NULL, NULL, '5', '150');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('435', '3', NULL, NULL, '5', '150');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('535', '3', NULL, NULL, '5', '150');


INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('145', '4', NULL, NULL, '5', '170');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('245', '4', NULL, NULL, '5', '170');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('345', '4', NULL, NULL, '5', '170');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('445', '4', NULL, NULL, '5', '170');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('545', '4', NULL, NULL, '5', '170');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('645', '4', NULL, NULL, '5', '170');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('745', '4', NULL, NULL, '5', '170');


INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('155', '5', NULL, NULL, '5', '120');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('255', '5', NULL, NULL, '5', '120');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('355', '5', NULL, NULL, '5', '120');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('455', '5', NULL, NULL, '5', '120');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('555', '5', NULL, NULL, '5', '120');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('655', '5', NULL, NULL, '5', '120');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('755', '5', NULL, NULL, '5', '120');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('855', '5', NULL, NULL, '5', '120');
INSERT INTO Jednotlive_kusy (jednotlive_kusy_key, obcerstveni_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('955', '5', NULL, NULL, '5', '120');


INSERT INTO caj (caj_key, nazev, druh, puvod) VALUES ('1', 'Tamaryokucha kyoto', 'zelený čaj', 'Japonsko');
INSERT INTO caj (caj_key, nazev, druh, puvod) VALUES ('2', 'Zhu cha', 'zelený čaj', 'Čína');
INSERT INTO caj (caj_key, nazev, druh, puvod) VALUES ('3', 'Qi hong mao feng cha', 'červený čaj', 'Čína');
INSERT INTO caj (caj_key, nazev, druh, puvod) VALUES ('4', 'Jing mao feng', 'červený čaj', 'Čína');
INSERT INTO caj (caj_key, nazev, druh, puvod) VALUES ('5', 'Rize cay', 'černý čaj', 'Turecko');
INSERT INTO caj (caj_key, nazev, druh, puvod) VALUES ('6', 'Liu bao bing cha', 'tmavý čaj', 'Čína');
INSERT INTO caj (caj_key, nazev, druh, puvod) VALUES ('7', 'Vzpomínka na Bombay', 'černý čaj', 'Indie');
INSERT INTO caj (caj_key, nazev, druh, puvod) VALUES ('8', 'Man-cha', NULL, 'Arábie');
INSERT INTO caj (caj_key, nazev, druh, puvod) VALUES ('9', 'Ledovýtouareg', 'zelený čaj', 'Čína');
INSERT INTO caj (caj_key, nazev, druh, puvod) VALUES ('10', 'Kyoto kinen', 'zelený čaj', 'Japonsko');


INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('111', '1', NULL, NULL, '1', '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('211', '1', NULL, NULL, '1', '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('311', '1', NULL, NULL, '1', '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('411', '1', NULL, NULL, '1', '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('511', '1', NULL, NULL, '1', '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('611', '1', NULL, NULL, '1', '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('711', '1', NULL, NULL, '1', '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('811', '1', NULL, NULL, '1', '3');



INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('121', '2', NULL, NULL, '1', '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('221', '2', NULL, NULL, '1', '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('321', '2', NULL, NULL, '1', '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('421', '2', NULL, NULL, '1', '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('521', '2', NULL, NULL, '1', '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('621', '2', NULL, NULL, '1', '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('721', '2', NULL, NULL, '1', '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('821', '2', NULL, NULL, '1', '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('921', '2', NULL, NULL, '1', '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('1021', '2', NULL, NULL, '1', '2');


INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('131', '3', NULL, NULL, '1', '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('231', '3', NULL, NULL, '1', '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('331', '3', NULL, NULL, '1', '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('431', '3', NULL, NULL, '1', '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('531', '3', NULL, NULL, '1', '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('631', '3', NULL, NULL, '1', '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('731', '3', NULL, NULL, '1', '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('831', '3', NULL, NULL, '1', '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('931', '3', NULL, NULL, '1', '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('1031', '3', NULL, NULL, '1', '2');


INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('141', '4', NULL, NULL, '1', '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('241', '4', NULL, NULL, '1', '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('341', '4', NULL, NULL, '1', '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('441', '4', NULL, NULL, '1', '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('541', '4', NULL, NULL, '1', '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('641', '4', NULL, NULL, '1', '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('741', '4', NULL, NULL, '1', '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('841', '4', NULL, NULL, '1', '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('941', '4', NULL, NULL, '1', '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('1041', '4', NULL, NULL, '1', '3');


INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('151', '5', NULL, NULL, '1', '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('251', '5', NULL, NULL, '1', '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('351', '5', NULL, NULL, '1', '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('451', '5', NULL, NULL, '1', '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('551', '5', NULL, NULL, '1', '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('651', '5', NULL, NULL, '1', '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('751', '5', NULL, NULL, '1', '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('851', '5', NULL, NULL, '1', '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('951', '5', NULL, NULL, '1', '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('1051', '5', NULL, NULL, '1', '4');


INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('161', '6', NULL, NULL, '1', '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('261', '6', NULL, NULL, '1', '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('361', '6', NULL, NULL, '1', '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('461', '6', NULL, NULL, '1', '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('561', '6', NULL, NULL, '1', '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('661', '6', NULL, NULL, '1', '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('761', '6', NULL, NULL, '1', '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('861', '6', NULL, NULL, '1', '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('961', '6', NULL, NULL, '1', '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('1061', '6', NULL, NULL, '1', '4');

INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('171', '7', '3', NULL, NULL, '5');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('271', '7', '3', NULL, NULL, '5');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('371', '7', '3', NULL, NULL, '5');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('471', '7', '3', NULL, NULL, '5');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('571', '7', NULL, NULL, '1', '5');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('671', '7', NULL, NULL, '1', '5');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('771', '7', NULL, NULL, '1', '5');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('871', '7', NULL, NULL, '1', '5');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('971', '7', NULL, NULL, '1', '5');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('1071', '7', NULL, NULL, '1', '5');


INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('181', '8', NULL, NULL, '1', '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('281', '8', NULL, NULL, '1', '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('381', '8', NULL, NULL, '1', '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('481', '8', NULL, NULL, '1', '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('581', '8', NULL, NULL, '1', '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('681', '8', NULL, NULL, '1', '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('781', '8', NULL, NULL, '1', '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('881', '8', NULL, NULL, '1', '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('981', '8', NULL, NULL, '1', '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('1081', '8', NULL, NULL, '1', '2');


INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('191', '9', NULL, NULL, '1', '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('291', '9', NULL, NULL, '1', '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('391', '9', NULL, NULL, '1', '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('491', '9', NULL, NULL, '1', '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('591', '9', NULL, NULL, '1', '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('691', '9', NULL, NULL, '1', '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('791', '9', NULL, NULL, '1', '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('891', '9', NULL, NULL, '1', '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('991', '9', NULL, NULL, '1', '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('1091', '9', NULL, NULL, '1', '3');


INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('1101', '10', NULL, NULL, '1', '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('2101', '10', NULL, NULL, '1', '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('3101', '10', NULL, NULL, '1', '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('4101', '10', NULL, NULL, '1', '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('5101', '10', NULL, NULL, '1', '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('6101', '10', NULL, NULL, '1', '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('7101', '10', NULL, NULL, '1', '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('8101', '10', NULL, NULL, '1', '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('9101', '10', NULL, NULL, '1', '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('10101', '10', NULL, NULL, '1', '3');


INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('112', '1', NULL, NULL, '2', '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('212', '1', NULL, NULL, '2', '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('312', '1', NULL, NULL, '2', '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('412', '1', NULL, NULL, '2', '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('512', '1', NULL, NULL, '2', '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('612', '1', NULL, NULL, '2', '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('712', '1', NULL, NULL, '2', '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('812', '1', NULL, NULL, '2', '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('912', '1', NULL, NULL, '2', '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('1012', '1', NULL, NULL, '2', '3');


INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('132', '3', NULL, '3', NULL, '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('232', '3', NULL, NULL, '2', '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('332', '3', NULL, NULL, '2', '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('432', '3', NULL, NULL, '2', '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('532', '3', NULL, NULL, '2', '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('632', '3', NULL, NULL, '2', '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('732', '3', NULL, NULL, '2', '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('832', '3', NULL, NULL, '2', '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('932', '3', NULL, NULL, '2', '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('1032', '3', NULL, NULL, '2', '2');


INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('142', '4', NULL, '3', NULL, '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('242', '4', NULL, NULL, '2', '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('342', '4', NULL, NULL, '2', '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('442', '4', NULL, NULL, '2', '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('542', '4', NULL, NULL, '2', '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('642', '4', NULL, NULL, '2', '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('742', '4', NULL, NULL, '2', '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('842', '4', NULL, NULL, '2', '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('942', '4', NULL, NULL, '2', '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('1042', '4', NULL, NULL, '2', '3');


INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('152', '5', NULL, NULL, '2', '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('252', '5', NULL, NULL, '2', '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('352', '5', NULL, NULL, '2', '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('452', '5', NULL, NULL, '2', '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('552', '5', NULL, NULL, '2', '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('652', '5', NULL, NULL, '2', '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('752', '5', NULL, NULL, '2', '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('852', '5', NULL, NULL, '2', '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('952', '5', NULL, NULL, '2', '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('1052', '5', NULL, NULL, '2', '4');


INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('162', '6', NULL, '3', NULL, '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('262', '6', NULL, NULL, '2', '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('362', '6', NULL, NULL, '2', '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('462', '6', NULL, NULL, '2', '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('562', '6', NULL, NULL, '2', '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('662', '6', NULL, NULL, '2', '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('762', '6', NULL, NULL, '2', '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('862', '6', NULL, NULL, '2', '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('962', '6', NULL, NULL, '2', '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('1062', '6', NULL, NULL, '2', '4');

INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('172', '7', NULL, NULL, '2', '5');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('272', '7', NULL, NULL, '2', '5');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('372', '7', NULL, NULL, '2', '5');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('472', '7', NULL, NULL, '2', '5');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('572', '7', NULL, NULL, '2', '5');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('672', '7', NULL, NULL, '2', '5');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('772', '7', NULL, NULL, '2', '5');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('872', '7', NULL, NULL, '2', '5');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('972', '7', NULL, NULL, '2', '5');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('1072', '7', NULL, NULL, '2', '5');


INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('182', '8', NULL, NULL, '2', '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('282', '8', NULL, NULL, '2', '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('382', '8', NULL, NULL, '2', '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('482', '8', NULL, NULL, '2', '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('582', '8', NULL, NULL, '2', '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('682', '8', NULL, NULL, '2', '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('782', '8', NULL, NULL, '2', '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('882', '8', NULL, NULL, '2', '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('982', '8', NULL, NULL, '2', '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('1082', '8', NULL, NULL, '2', '2');


INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('1102', '10', NULL, NULL, '2', '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('2102', '10', NULL, NULL, '2', '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('3102', '10', NULL, NULL, '2', '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('4102', '10', NULL, NULL, '2', '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('5102', '10', NULL, NULL, '2', '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('6102', '10', NULL, NULL, '2', '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('7102', '10', NULL, NULL, '2', '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('8102', '10', NULL, NULL, '2', '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('9102', '10', NULL, NULL, '2', '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('10102', '10', NULL, NULL, '2', '3');


INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('113', '1', NULL, NULL, '3',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('213', '1', NULL, NULL, '3',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('313', '1', NULL, NULL, '3',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('413', '1', NULL, NULL, '3',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('513', '1', NULL, NULL, '3',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('613', '1', NULL, NULL, '3',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('713', '1', NULL, NULL, '3',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('813', '1', NULL, NULL, '3',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('913', '1', NULL, NULL, '3',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('1013', '1', NULL, NULL, '3',  '3');


INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('123', '2', NULL, '2', NULL,  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('223', '2', NULL, NULL, '3',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('323', '2', NULL, NULL, '3',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('423', '2', NULL, NULL, '3',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('523', '2', NULL, NULL, '3',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('623', '2', NULL, NULL, '3',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('723', '2', NULL, NULL, '3',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('823', '2', NULL, NULL, '3',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('923', '2', NULL, NULL, '3',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('1023', '2', NULL, NULL, '3',  '2');


INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('133', '3', NULL, '2', NULL,  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('233', '3', NULL, NULL, '3',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('333', '3', NULL, NULL, '3',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('433', '3', NULL, NULL, '3',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('533', '3', NULL, NULL, '3',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('633', '3', NULL, NULL, '3',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('733', '3', NULL, NULL, '3',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('833', '3', NULL, NULL, '3',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('933', '3', NULL, NULL, '3',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('1033', '3', NULL, NULL, '3',  '2');


INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('143', '4', NULL, '2', NULL,  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('243', '4', NULL, NULL, '3',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('343', '4', NULL, NULL, '3',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('443', '4', NULL, NULL, '3',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('543', '4', NULL, NULL, '3',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('643', '4', NULL, NULL, '3',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('743', '4', NULL, NULL, '3',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('843', '4', NULL, NULL, '3',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('943', '4', NULL, NULL, '3',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('1043', '4', NULL, NULL, '3',  '3');


INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('153', '5', NULL, NULL, '3',  '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('253', '5', NULL, NULL, '3',  '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('353', '5', NULL, NULL, '3',  '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('453', '5', NULL, NULL, '3',  '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('553', '5', NULL, NULL, '3',  '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('653', '5', NULL, NULL, '3',  '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('753', '5', NULL, NULL, '3',  '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('853', '5', NULL, NULL, '3',  '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('953', '5', NULL, NULL, '3',  '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('1053', '5', NULL, NULL, '3',  '4');


INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('163', '6', NULL, NULL, '3',  '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('263', '6', NULL, NULL, '3',  '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('363', '6', NULL, NULL, '3',  '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('463', '6', NULL, NULL, '3',  '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('563', '6', NULL, NULL, '3',  '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('663', '6', NULL, NULL, '3',  '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('763', '6', NULL, NULL, '3',  '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('863', '6', NULL, NULL, '3',  '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('963', '6', NULL, NULL, '3',  '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('1063', '6', NULL, NULL, '3',  '4');

INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('173', '7', NULL, NULL, '3',  '5');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('273', '7', NULL, NULL, '3',  '5');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('373', '7', NULL, NULL, '3',  '5');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('473', '7', NULL, NULL, '3',  '5');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('573', '7', NULL, NULL, '3',  '5');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('673', '7', NULL, NULL, '3',  '5');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('773', '7', NULL, NULL, '3',  '5');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('873', '7', NULL, NULL, '3',  '5');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('973', '7', NULL, NULL, '3',  '5');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('1073', '7', NULL, NULL, '3',  '5');


INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('183', '8', NULL, NULL, '3',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('283', '8', NULL, NULL, '3',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('383', '8', NULL, NULL, '3',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('483', '8', NULL, NULL, '3',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('583', '8', NULL, NULL, '3',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('683', '8', NULL, NULL, '3',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('783', '8', NULL, NULL, '3',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('883', '8', NULL, NULL, '3',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('983', '8', NULL, NULL, '3',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('1083', '8', NULL, NULL, '3',  '2');


INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('193', '9', NULL, NULL, '3',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('293', '9', NULL, NULL, '3',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('393', '9', NULL, NULL, '3',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('493', '9', NULL, NULL, '3',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('593', '9', NULL, NULL, '3',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('693', '9', NULL, NULL, '3',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('793', '9', NULL, NULL, '3',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('893', '9', NULL, NULL, '3',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('993', '9', NULL, NULL, '3',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('1093', '9', NULL, NULL, '3',  '3');


INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('1103', '10', NULL, NULL, '3',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('2103', '10', NULL, NULL, '3',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('3103', '10', NULL, NULL, '3',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('4103', '10', NULL, NULL, '3',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('5103', '10', NULL, NULL, '3',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('6103', '10', NULL, NULL, '3',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('7103', '10', NULL, NULL, '3',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('8103', '10', NULL, NULL, '3',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('9103', '10', NULL, NULL, '3',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('10103', '10', NULL, NULL, '3',  '3');


INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('114', '1', NULL, NULL, '4',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('214', '1', NULL, NULL, '4',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('314', '1', NULL, NULL, '4',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('414', '1', NULL, NULL, '4',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('514', '1', NULL, NULL, '4',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('614', '1', NULL, NULL, '4',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('714', '1', NULL, NULL, '4',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('814', '1', NULL, NULL, '4',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('914', '1', NULL, NULL, '4',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('1014', '1', NULL, NULL, '4',  '3');


INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('124', '2', NULL, NULL, '4',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('224', '2', NULL, NULL, '4',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('324', '2', NULL, NULL, '4',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('424', '2', NULL, NULL, '4',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('524', '2', NULL, NULL, '4',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('624', '2', NULL, NULL, '4',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('724', '2', NULL, NULL, '4',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('824', '2', NULL, NULL, '4',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('924', '2', NULL, NULL, '4',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('1024', '2', NULL, NULL, '4',  '2');


INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('134', '3', NULL, NULL, '4',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('234', '3', NULL, NULL, '4',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('334', '3', NULL, NULL, '4',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('434', '3', NULL, NULL, '4',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('534', '3', NULL, NULL, '4',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('634', '3', NULL, NULL, '4',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('734', '3', NULL, NULL, '4',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('834', '3', NULL, NULL, '4',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('934', '3', NULL, NULL, '4',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('1034', '3', NULL, NULL, '4',  '2');


INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('144', '4', NULL, NULL, '4',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('244', '4', NULL, NULL, '4',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('344', '4', NULL, NULL, '4',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('444', '4', NULL, NULL, '4',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('544', '4', NULL, NULL, '4',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('644', '4', NULL, NULL, '4',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('744', '4', NULL, NULL, '4',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('844', '4', NULL, NULL, '4',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('944', '4', NULL, NULL, '4',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('1044', '4', NULL, NULL, '4',  '3');


INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('154', '5', '2', NULL, NULL,  '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('254', '5', '2', NULL, NULL,  '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('354', '5', NULL, NULL, '4',  '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('454', '5', NULL, NULL, '4',  '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('554', '5', NULL, NULL, '4',  '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('654', '5', NULL, NULL, '4',  '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('754', '5', NULL, NULL, '4',  '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('854', '5', NULL, NULL, '4',  '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('954', '5', NULL, NULL, '4',  '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('1054', '5', NULL, NULL, '4',  '4');


INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('164', '6', NULL, NULL, '4',  '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('264', '6', NULL, NULL, '4',  '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('364', '6', NULL, NULL, '4',  '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('464', '6', NULL, NULL, '4',  '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('564', '6', NULL, NULL, '4',  '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('664', '6', NULL, NULL, '4',  '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('764', '6', NULL, NULL, '4',  '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('864', '6', NULL, NULL, '4',  '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('964', '6', NULL, NULL, '4',  '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('1064', '6', NULL, NULL, '4',  '4');

INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('174', '7', NULL, NULL, '4',  '5');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('274', '7', NULL, NULL, '4',  '5');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('374', '7', NULL, NULL, '4',  '5');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('474', '7', NULL, NULL, '4',  '5');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('574', '7', NULL, NULL, '4',  '5');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('674', '7', NULL, NULL, '4',  '5');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('774', '7', NULL, NULL, '4',  '5');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('874', '7', NULL, NULL, '4',  '5');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('974', '7', NULL, NULL, '4',  '5');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('1074', '7', NULL, NULL, '4',  '5');


INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('184', '8', NULL, NULL, '4',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('284', '8', NULL, NULL, '4',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('384', '8', NULL, NULL, '4',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('484', '8', NULL, NULL, '4',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('584', '8', NULL, NULL, '4',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('684', '8', NULL, NULL, '4',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('784', '8', NULL, NULL, '4',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('884', '8', NULL, NULL, '4',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('984', '8', NULL, NULL, '4',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('1084', '8', NULL, NULL, '4',  '2');


INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('194', '9', NULL, NULL, '4',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('294', '9', NULL, NULL, '4',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('394', '9', NULL, NULL, '4',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('494', '9', NULL, NULL, '4',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('594', '9', NULL, NULL, '4',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('694', '9', NULL, NULL, '4',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('794', '9', NULL, NULL, '4',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('894', '9', NULL, NULL, '4',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('994', '9', NULL, NULL, '4',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('1094', '9', NULL, NULL, '4',  '3');


INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('1104', '10', '2', NULL, NULL,  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('2104', '10', '2', NULL, NULL,  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('3104', '10', '2', NULL, NULL,  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('4104', '10', '2', NULL, NULL,  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('5104', '10', NULL, NULL, '4',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('6104', '10', NULL, NULL, '4',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('7104', '10', NULL, NULL, '4',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('8104', '10', NULL, NULL, '4',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('9104', '10', NULL, NULL, '4',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('10104', '10', NULL, NULL, '4',  '3');


INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('115', '1', NULL, NULL, '5',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('215', '1', NULL, NULL, '5',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('315', '1', NULL, NULL, '5',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('415', '1', NULL, NULL, '5',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('515', '1', NULL, NULL, '5',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('615', '1', NULL, NULL, '5',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('715', '1', NULL, NULL, '5',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('815', '1', NULL, NULL, '5',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('915', '1', NULL, NULL, '5',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('1015', '1', NULL, NULL, '5',  '3');


INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('125', '2', NULL, '1', NULL,  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('225', '2', NULL, NULL, '5',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('325', '2', NULL, NULL, '5',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('425', '2', NULL, NULL, '5',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('525', '2', NULL, NULL, '5',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('625', '2', NULL, NULL, '5',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('725', '2', NULL, NULL, '5',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('825', '2', NULL, NULL, '5',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('925', '2', NULL, NULL, '5',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('1025', '2', NULL, NULL, '5',  '2');


INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('135', '3', '1', NULL, NULL,  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('235', '3', '1', NULL, NULL,  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('335', '3', '1', NULL, NULL,  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('435', '3', '1', NULL, NULL,  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('535', '3', '1', NULL, NULL,  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('635', '3', '1', NULL, NULL,  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('735', '3', NULL, NULL, '5',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('835', '3', NULL, NULL, '5',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('935', '3', NULL, NULL, '5',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('1035', '3', NULL, NULL, '5',  '2');


INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('145', '4', NULL, NULL, '5',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('245', '4', NULL, NULL, '5',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('345', '4', NULL, NULL, '5',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('445', '4', NULL, NULL, '5',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('545', '4', NULL, NULL, '5',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('645', '4', NULL, NULL, '5',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('745', '4', NULL, NULL, '5',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('845', '4', NULL, NULL, '5',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('945', '4', NULL, NULL, '5',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('1045', '4', NULL, NULL, '5',  '3');


INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('155', '5', NULL, '1', NULL,  '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('255', '5', NULL, NULL, '5',  '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('355', '5', NULL, NULL, '5',  '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('455', '5', NULL, NULL, '5',  '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('555', '5', NULL, NULL, '5',  '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('655', '5', NULL, NULL, '5',  '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('755', '5', NULL, NULL, '5',  '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('855', '5', NULL, NULL, '5',  '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('955', '5', NULL, NULL, '5',  '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('1055', '5', NULL, NULL, '5',  '4');


INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('165', '6', NULL, NULL, '5',  '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('265', '6', NULL, NULL, '5',  '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('365', '6', NULL, NULL, '5',  '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('465', '6', NULL, NULL, '5',  '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('565', '6', NULL, NULL, '5',  '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('665', '6', NULL, NULL, '5',  '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('765', '6', NULL, NULL, '5',  '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('865', '6', NULL, NULL, '5',  '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('965', '6', NULL, NULL, '5',  '4');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('1065', '6', NULL, NULL, '5',  '4');

INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('175', '7', NULL, '1', NULL,  '5');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('275', '7', NULL, '1', NULL,  '5');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('375', '7', NULL, NULL, '5',  '5');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('475', '7', NULL, NULL, '5',  '5');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('575', '7', NULL, NULL, '5',  '5');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('675', '7', NULL, NULL, '5',  '5');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('775', '7', NULL, NULL, '5',  '5');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('875', '7', NULL, NULL, '5',  '5');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('975', '7', NULL, NULL, '5',  '5');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('1075', '7', NULL, NULL, '5',  '5');


INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('185', '8', NULL, NULL, '5',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('285', '8', NULL, NULL, '5',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('385', '8', NULL, NULL, '5',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('485', '8', NULL, NULL, '5',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('585', '8', NULL, NULL, '5',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('685', '8', NULL, NULL, '5',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('785', '8', NULL, NULL, '5',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('885', '8', NULL, NULL, '5',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('985', '8', NULL, NULL, '5',  '2');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('1085', '8', NULL, NULL, '5',  '2');


INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('195', '9', NULL, NULL, '5',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('295', '9', NULL, NULL, '5',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('395', '9', NULL, NULL, '5',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('495', '9', NULL, NULL, '5',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('595', '9', NULL, NULL, '5',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('695', '9', NULL, NULL, '5',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('795', '9', NULL, NULL, '5',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('895', '9', NULL, NULL, '5',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('995', '9', NULL, NULL, '5',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('1095', '9', NULL, NULL, '5',  '3');


INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('1105', '10', NULL, '4', NULL,  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('2105', '10', NULL, '4', NULL,  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('3105', '10', NULL, NULL, '5',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('4105', '10', NULL, NULL, '5',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('5105', '10', NULL, NULL, '5',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('6105', '10', NULL, NULL, '5',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('7105', '10', NULL, NULL, '5',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('8105', '10', NULL, NULL, '5',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('9105', '10', NULL, NULL, '5',  '3');
INSERT INTO Jednotlive_porce (jednotlive_porce_key, caj_key, zbozi_key, objednavka_key, pobocka_key, gramaz) VALUES ('10105', '10', NULL, NULL, '5',  '3');


ALTER SESSION SET nls_date_format = 'DD.MM.YYYY hh24:mi:ss';


CREATE SEQUENCE SEQ_rezervace_key START WITH 1 INCREMENT BY 1;

INSERT INTO rezervace (rezervace_key, pobocka_key, cislo, cas, datum, prijmeni, jmeno) VALUES (SEQ_rezervace_key.NEXTVAL, '1', '1', to_date('25.05.2018 18:30:00', 'DD.MM.YYYY hh24:mi:ss'), to_date('25.05.2018', 'dd.mm.yyyy'), 'Pánková', 'Ljuba');
INSERT INTO rezervace (rezervace_key, pobocka_key, cislo, cas, datum, prijmeni, jmeno) VALUES (SEQ_rezervace_key.NEXTVAL, '1', '2', to_date('15.05.2018 19:30:00', 'DD.MM.YYYY hh24:mi:ss'), to_date('15.05.2018', 'dd.mm.yyyy'), 'Zeman', 'Milan');
INSERT INTO rezervace (rezervace_key, pobocka_key, cislo, cas, datum, prijmeni, jmeno) VALUES (SEQ_rezervace_key.NEXTVAL, '1', '10', to_date('28.05.2018 19:30:00', 'DD.MM.YYYY hh24:mi:ss'),  to_date('28.05.2018', 'dd.mm.yyyy'), 'Čeřovský', 'Ludvík');
INSERT INTO rezervace (rezervace_key, pobocka_key, cislo, cas, datum, prijmeni, jmeno) VALUES (SEQ_rezervace_key.NEXTVAL, '2', '5', to_date('12.05.2018 14:30:00', 'DD.MM.YYYY hh24:mi:ss'), to_date('12.05.2018', 'dd.mm.yyyy'), 'Srbová', 'Aneta');
INSERT INTO rezervace (rezervace_key, pobocka_key, cislo, cas, datum, prijmeni, jmeno) VALUES (SEQ_rezervace_key.NEXTVAL, '3', '8',  to_date('10.05.2018 14:00:00', 'DD.MM.YYYY hh24:mi:ss'), to_date('10.05.2018', 'dd.mm.yyyy'), 'Schmid', 'Norbert');
INSERT INTO rezervace (rezervace_key, pobocka_key, cislo, cas, datum, prijmeni, jmeno) VALUES (SEQ_rezervace_key.NEXTVAL, '3', '1',  to_date('16.05.2018 14:45:00', 'DD.MM.YYYY hh24:mi:ss'), to_date('16.05.2018', 'dd.mm.yyyy'), 'Niklová', 'Soňa');
INSERT INTO rezervace (rezervace_key, pobocka_key, cislo, cas, datum, prijmeni, jmeno) VALUES (SEQ_rezervace_key.NEXTVAL, '4', '6',  to_date('1.05.2018 18:00:00', 'DD.MM.YYYY hh24:mi:ss'), to_date('1.05.2018', 'dd.mm.yyyy'), 'Palová', 'Milada');
INSERT INTO rezervace (rezervace_key, pobocka_key, cislo, cas, datum, prijmeni, jmeno) VALUES (SEQ_rezervace_key.NEXTVAL, '5', '7',  to_date('20.05.2018 14:00:00', 'DD.MM.YYYY hh24:mi:ss'), to_date('20.05.2018', 'dd.mm.yyyy'), 'Sůvová', 'Milena');
INSERT INTO rezervace (rezervace_key, pobocka_key, cislo, cas, datum, prijmeni, jmeno) VALUES (SEQ_rezervace_key.NEXTVAL, '5', '9',  to_date('13.05.2018 17:00:00', 'DD.MM.YYYY hh24:mi:ss'), to_date('13.05.2018', 'dd.mm.yyyy'), 'Trčková', 'Marie');

INSERT INTO objednavka (objednavka_key) VALUES('1');
INSERT INTO objednavka (objednavka_key) VALUES('2');
INSERT INTO objednavka (objednavka_key) VALUES('3');
INSERT INTO objednavka (objednavka_key) VALUES('4');

INSERT INTO Vodni_dymka (vodni_dymka_key, zbozi_key, objednavka_key, pobocka_key, typ, vyska) VALUES ('13', NULL,'2', NULL, 'a', '50' );
INSERT INTO Vodni_dymka (vodni_dymka_key, zbozi_key, objednavka_key, pobocka_key, typ, vyska) VALUES ('22', NULL,'3', NULL, 'b', '60' );
INSERT INTO Vodni_dymka (vodni_dymka_key, zbozi_key, objednavka_key, pobocka_key, typ, vyska) VALUES ('31', NULL, NULL, '1', 'c', '70' );
INSERT INTO Vodni_dymka (vodni_dymka_key, zbozi_key, objednavka_key, pobocka_key, typ, vyska) VALUES ('42', NULL, NULL, '2', 'd', '50' );
INSERT INTO Vodni_dymka (vodni_dymka_key, zbozi_key, objednavka_key, pobocka_key, typ, vyska) VALUES ('53', NULL, NULL, '3', 'a', '60' );
INSERT INTO Vodni_dymka (vodni_dymka_key, zbozi_key, objednavka_key, pobocka_key, typ, vyska) VALUES ('64', NULL, NULL, '4', 'a', '50' );
INSERT INTO Vodni_dymka (vodni_dymka_key, zbozi_key, objednavka_key, pobocka_key, typ, vyska) VALUES ('75', NULL, NULL, '5', 'a', '50' );

INSERT INTO Zbozi (zbozi_key, cena) values ('1', '152');
INSERT INTO Zbozi (zbozi_key, cena) values ('2', '145');
INSERT INTO Zbozi (zbozi_key, cena) values ('3', '246');
INSERT INTO Zbozi (zbozi_key, cena) values ('4', '128');

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '11', '1', '150', 'Nadobi');
INSERT INTO nadobi (vybaveni_key, pobocka_key, material) VALUES  ('11', '1', 'Keramika');

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '21', '1', '150', 'Nadobi');
INSERT INTO nadobi (vybaveni_key, pobocka_key, material) VALUES  ('21', '1', 'Keramika');

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '31', '1', '200', 'Nadobi');
INSERT INTO nadobi (vybaveni_key, pobocka_key, material) VALUES  ('31', '1', 'Sklo');

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '41', '1', '200', 'Nadobi');
INSERT INTO nadobi (vybaveni_key, pobocka_key, material) VALUES  ('41', '1', 'Sklo');

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '51', '1', '170', 'Nadobi');
INSERT INTO nadobi (vybaveni_key, pobocka_key, material) VALUES  ('51', '1', NULL);

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '61', '1', '170', 'Nadobi');
INSERT INTO nadobi (vybaveni_key, pobocka_key, material) VALUES  ('61', '1', NULL);

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '71', '1', '350', 'Nadobi');
INSERT INTO nadobi (vybaveni_key, pobocka_key, material) VALUES  ('71', '1', 'Keramika');

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '81', '1', '350', 'Nadobi');
INSERT INTO nadobi (vybaveni_key, pobocka_key, material) VALUES  ('81', '1', 'Keramika');

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '91', '1', '150', 'Nabytek');
INSERT INTO Nabytek (vybaveni_key, pobocka_key, opravy) VALUES  ('91', '1', NULL);

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '101', '1', '150', 'Nabytek');
INSERT INTO Nabytek (vybaveni_key, pobocka_key, opravy) VALUES  ('101', '1', NULL);

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '111', '1', '200', 'Nabytek');
INSERT INTO Nabytek (vybaveni_key, pobocka_key, opravy) VALUES  ('111', '1', NULL);

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '121', '1', '200', 'Nabytek');
INSERT INTO Nabytek (vybaveni_key, pobocka_key, opravy) VALUES  ('121', '1', NULL);

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '131', '1', '170', 'Nabytek');
INSERT INTO Nabytek (vybaveni_key, pobocka_key, opravy) VALUES  ('131', '1', NULL);

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '141', '1', '170', 'Nabytek');
INSERT INTO Nabytek (vybaveni_key, pobocka_key, opravy) VALUES  ('141', '1', NULL);

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '151', '1', '350', 'Nabytek');
INSERT INTO Nabytek (vybaveni_key, pobocka_key, opravy) VALUES  ('151', '1', NULL);

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '161', '1', '350', 'Nabytek');
INSERT INTO Nabytek (vybaveni_key, pobocka_key, opravy) VALUES  ('161', '1', NULL);


INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '12', '2', '150', 'Nadobi');
INSERT INTO nadobi (vybaveni_key, pobocka_key, material) VALUES  ('12', '2', 'Keramika');

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '22', '2', '150', 'Nadobi');
INSERT INTO nadobi (vybaveni_key, pobocka_key, material) VALUES  ('22', '2', 'Keramika');

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '32', '2', '200', 'Nadobi');
INSERT INTO nadobi (vybaveni_key, pobocka_key, material) VALUES  ('32', '2', 'Sklo');

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '42', '2', '200', 'Nadobi');
INSERT INTO nadobi (vybaveni_key, pobocka_key, material) VALUES  ('42', '2', 'Sklo');

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '52', '2', '170', 'Nadobi');
INSERT INTO nadobi (vybaveni_key, pobocka_key, material) VALUES  ('52', '2', NULL);

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '62', '2', '170', 'Nadobi');
INSERT INTO nadobi (vybaveni_key, pobocka_key, material) VALUES  ('62', '2', NULL);

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '72', '2', '350', 'Nadobi');
INSERT INTO nadobi (vybaveni_key, pobocka_key, material) VALUES  ('72', '2', 'Keramika');

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '82', '2', '350', 'Nadobi');
INSERT INTO nadobi (vybaveni_key, pobocka_key, material) VALUES  ('82', '2', 'Keramika');

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '92', '2', '150', 'Nabytek');
INSERT INTO Nabytek (vybaveni_key, pobocka_key, opravy) VALUES  ('92', '2', NULL);

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '102', '2', '150', 'Nabytek');
INSERT INTO Nabytek (vybaveni_key, pobocka_key, opravy) VALUES  ('102', '2', NULL);

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '112', '2', '200', 'Nabytek');
INSERT INTO Nabytek (vybaveni_key, pobocka_key, opravy) VALUES  ('112', '2', NULL);

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '122', '2', '200', 'Nabytek');
INSERT INTO Nabytek (vybaveni_key, pobocka_key, opravy) VALUES  ('122', '2', NULL);

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '132', '2', '170', 'Nabytek');
INSERT INTO Nabytek (vybaveni_key, pobocka_key, opravy) VALUES  ('132', '2', NULL);

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '142', '2', '170', 'Nabytek');
INSERT INTO Nabytek (vybaveni_key, pobocka_key, opravy) VALUES  ('142', '2', NULL);

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '152', '2', '350', 'Nabytek');
INSERT INTO Nabytek (vybaveni_key, pobocka_key, opravy) VALUES  ('152', '2', NULL);

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '162', '2', '350', 'Nabytek');
INSERT INTO Nabytek (vybaveni_key, pobocka_key, opravy) VALUES  ('162', '2', NULL);

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '13', '3', '150', 'Nadobi');
INSERT INTO nadobi (vybaveni_key, pobocka_key, material) VALUES  ('13', '3', 'Keramika');

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '23', '3', '150', 'Nadobi');
INSERT INTO nadobi (vybaveni_key, pobocka_key, material) VALUES  ('23', '3', 'Keramika');

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '33', '3', '200', 'Nadobi');
INSERT INTO nadobi (vybaveni_key, pobocka_key, material) VALUES  ('33', '3', 'Sklo');

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '43', '3', '200', 'Nadobi');
INSERT INTO nadobi (vybaveni_key, pobocka_key, material) VALUES  ('43', '3', 'Sklo');

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '53', '3', '170', 'Nadobi');
INSERT INTO nadobi (vybaveni_key, pobocka_key, material) VALUES  ('53', '3', NULL);

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '63', '3', '170', 'Nadobi');
INSERT INTO nadobi (vybaveni_key, pobocka_key, material) VALUES  ('63', '3', NULL);

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '73', '3', '350', 'Nadobi');
INSERT INTO nadobi (vybaveni_key, pobocka_key, material) VALUES  ('73', '3', 'Keramika');

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '83', '3', '350', 'Nadobi');
INSERT INTO nadobi (vybaveni_key, pobocka_key, material) VALUES  ('83', '3', 'Keramika');

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '93', '3', '150', 'Nabytek');
INSERT INTO Nabytek (vybaveni_key, pobocka_key, opravy) VALUES  ('93', '3', NULL);

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '103', '3', '150', 'Nabytek');
INSERT INTO Nabytek (vybaveni_key, pobocka_key, opravy) VALUES  ('103', '3', NULL);

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '113', '3', '200', 'Nabytek');
INSERT INTO Nabytek (vybaveni_key, pobocka_key, opravy) VALUES  ('113', '3', NULL);

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '123', '3', '200', 'Nabytek');
INSERT INTO Nabytek (vybaveni_key, pobocka_key, opravy) VALUES  ('123', '3', NULL);

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '133', '3', '170', 'Nabytek');
INSERT INTO Nabytek (vybaveni_key, pobocka_key, opravy) VALUES  ('133', '3', NULL);

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '143', '3', '170', 'Nabytek');
INSERT INTO Nabytek (vybaveni_key, pobocka_key, opravy) VALUES  ('143', '3', NULL);

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '153', '3', '350', 'Nabytek');
INSERT INTO Nabytek (vybaveni_key, pobocka_key, opravy) VALUES  ('153', '3', NULL);

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '163', '3', '350', 'Nabytek');
INSERT INTO Nabytek (vybaveni_key, pobocka_key, opravy) VALUES  ('163', '3', NULL);

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '14', '4', '150', 'Nadobi');
INSERT INTO nadobi (vybaveni_key, pobocka_key, material) VALUES  ('14', '4', 'Keramika');

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '24', '4', '150', 'Nadobi');
INSERT INTO nadobi (vybaveni_key, pobocka_key, material) VALUES  ('24', '4', 'Keramika');

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '34', '4', '200', 'Nadobi');
INSERT INTO nadobi (vybaveni_key, pobocka_key, material) VALUES  ('34', '4', 'Sklo');

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '44', '4', '200', 'Nadobi');
INSERT INTO nadobi (vybaveni_key, pobocka_key, material) VALUES  ('44', '4', 'Sklo');

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '54', '4', '170', 'Nadobi');
INSERT INTO nadobi (vybaveni_key, pobocka_key, material) VALUES  ('54', '4', NULL);

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '64', '4', '170', 'Nadobi');
INSERT INTO nadobi (vybaveni_key, pobocka_key, material) VALUES  ('64', '4', NULL);

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '74', '4', '350', 'Nadobi');
INSERT INTO nadobi (vybaveni_key, pobocka_key, material) VALUES  ('74', '4', 'Keramika');

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '84', '4', '350', 'Nadobi');
INSERT INTO nadobi (vybaveni_key, pobocka_key, material) VALUES  ('84', '4', 'Keramika');

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '94', '4', '150', 'Nabytek');
INSERT INTO Nabytek (vybaveni_key, pobocka_key, opravy) VALUES  ('94', '4', NULL);

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '104', '4', '150', 'Nabytek');
INSERT INTO Nabytek (vybaveni_key, pobocka_key, opravy) VALUES  ('104', '4', NULL);

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '114', '4', '200', 'Nabytek');
INSERT INTO Nabytek (vybaveni_key, pobocka_key, opravy) VALUES  ('114', '4', NULL);

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '124', '4', '200', 'Nabytek');
INSERT INTO Nabytek (vybaveni_key, pobocka_key, opravy) VALUES  ('124', '4', NULL);

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '134', '4', '170', 'Nabytek');
INSERT INTO Nabytek (vybaveni_key, pobocka_key, opravy) VALUES  ('134', '4', NULL);

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '144', '4', '170', 'Nabytek');
INSERT INTO Nabytek (vybaveni_key, pobocka_key, opravy) VALUES  ('144', '4', NULL);

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '154', '4', '350', 'Nabytek');
INSERT INTO Nabytek (vybaveni_key, pobocka_key, opravy) VALUES  ('154', '4', NULL);

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '164', '4', '350', 'Nabytek');
INSERT INTO Nabytek (vybaveni_key, pobocka_key, opravy) VALUES  ('164', '4', NULL);

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '15', '5', '150', 'Nadobi');
INSERT INTO nadobi (vybaveni_key, pobocka_key, material) VALUES  ('15', '5', 'Keramika');

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '25', '5', '150', 'Nadobi');
INSERT INTO nadobi (vybaveni_key, pobocka_key, material) VALUES  ('25', '5', 'Keramika');

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '35', '5', '200', 'Nadobi');
INSERT INTO nadobi (vybaveni_key, pobocka_key, material) VALUES  ('35', '5', 'Sklo');

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '45', '5', '200', 'Nadobi');
INSERT INTO nadobi (vybaveni_key, pobocka_key, material) VALUES  ('45', '5', 'Sklo');

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '55', '5', '170', 'Nadobi');
INSERT INTO nadobi (vybaveni_key, pobocka_key, material) VALUES  ('55', '5', NULL);

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '65', '5', '170', 'Nadobi');
INSERT INTO nadobi (vybaveni_key, pobocka_key, material) VALUES  ('65', '5', NULL);

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '75', '5', '350', 'Nadobi');
INSERT INTO nadobi (vybaveni_key, pobocka_key, material) VALUES  ('75', '5', 'Keramika');

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '85', '5', '350', 'Nadobi');
INSERT INTO nadobi (vybaveni_key, pobocka_key, material) VALUES  ('85', '5', 'Keramika');

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '95', '5', '150', 'Nabytek');
INSERT INTO Nabytek (vybaveni_key, pobocka_key, opravy) VALUES  ('95', '5', NULL);

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '105', '5', '150', 'Nabytek');
INSERT INTO Nabytek (vybaveni_key, pobocka_key, opravy) VALUES  ('105', '5', NULL);

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '115', '5', '200', 'Nabytek');
INSERT INTO Nabytek (vybaveni_key, pobocka_key, opravy) VALUES  ('115', '5', NULL);

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '125', '5', '200', 'Nabytek');
INSERT INTO Nabytek (vybaveni_key, pobocka_key, opravy) VALUES  ('125', '5', NULL);

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '135', '5', '170', 'Nabytek');
INSERT INTO Nabytek (vybaveni_key, pobocka_key, opravy) VALUES  ('135', '5', NULL);

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '145', '5', '170', 'Nabytek');
INSERT INTO Nabytek (vybaveni_key, pobocka_key, opravy) VALUES  ('145', '5', NULL);

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '155', '5', '350', 'Nabytek');
INSERT INTO Nabytek (vybaveni_key, pobocka_key, opravy) VALUES  ('155', '5', NULL);

INSERT INTO vybaveni (vybaveni_key, pobocka_key, cena, vybaveni_type) VALUES  ( '165', '5', '350', 'Nabytek');
INSERT INTO Nabytek (vybaveni_key, pobocka_key, opravy) VALUES  ('165', '5', NULL);

insert into objednavky (pobocka_key, objednavka_key, cislo) values ('5','1', '3');
insert into objednavky (pobocka_key, objednavka_key, cislo) values ('3','2', '9');
insert into objednavky (pobocka_key, objednavka_key, cislo) values ('2','3', '7');
insert into objednavky (pobocka_key, objednavka_key, cislo) values ('2','4', '1');

insert into predmety_dymka (predmety_dymka_key, zbozi_key, nazev) values ( '1', NULL, 'Tabak - malina');
insert into predmety_dymka (predmety_dymka_key, zbozi_key, nazev) values ( '2', NULL, 'Tabak - malina');
insert into predmety_dymka (predmety_dymka_key, zbozi_key, nazev) values ( '3', '1', 'Tabak - citrus');
insert into predmety_dymka (predmety_dymka_key, zbozi_key, nazev) values ( '4', '1', 'Tabak - citrus');
insert into predmety_dymka (predmety_dymka_key, zbozi_key, nazev) values ( '5', '2', 'Tabak - citrus');
insert into predmety_dymka (predmety_dymka_key, zbozi_key, nazev) values ( '6', NULL, 'Uhlik');
insert into predmety_dymka (predmety_dymka_key, zbozi_key, nazev) values ( '7', NULL, 'Uhlik');
insert into predmety_dymka (predmety_dymka_key, zbozi_key, nazev) values ( '8', NULL, 'Uhlik');
insert into predmety_dymka (predmety_dymka_key, zbozi_key, nazev) values ( '9', NULL, 'Uhlik');
insert into predmety_dymka (predmety_dymka_key, zbozi_key, nazev) values ( '10', '4', 'Uhlik');

insert into predmety_caj (predmety_key, zbozi_key, nazev ) values ( '1', '1', 'Konvička´- 0,5l');
insert into predmety_caj (predmety_key, zbozi_key, nazev ) values ( '2', '4', 'Konvička - 0,5l');
insert into predmety_caj (predmety_key, zbozi_key, nazev ) values ( '3', NULL, 'Konvička´- 1l');
insert into predmety_caj (predmety_key, zbozi_key, nazev ) values ( '4', NULL, 'Konvička - 1l');
insert into predmety_caj (predmety_key, zbozi_key, nazev ) values ( '5', NULL, 'Čajový filtr');
insert into predmety_caj (predmety_key, zbozi_key, nazev ) values ( '6', NULL, 'Čajový filtr');
insert into predmety_caj (predmety_key, zbozi_key, nazev ) values ( '7', NULL, 'Čajový filtr');
insert into predmety_caj (predmety_key, zbozi_key, nazev ) values ( '8', NULL, 'Šálek');
insert into predmety_caj (predmety_key, zbozi_key, nazev ) values ( '9', NULL, 'Šálek');
insert into predmety_caj (predmety_key, zbozi_key, nazev ) values ( '10', NULL, 'Šálek');


-- Dulezite je na konci zavolat commit!
commit;

prompt #----------------------#
prompt #- Zapnout cizi klice -#
prompt #----------------------#

exec ZAPNI_CIZI_KLICE;